/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowValueSource.java
 */

package rind.solr.plugin;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.queries.function.FunctionValues;
import org.apache.lucene.queries.function.ValueSource;
import org.apache.lucene.queries.function.docvalues.BoolDocValues;
import org.apache.lucene.queries.function.docvalues.DoubleDocValues;
import org.apache.lucene.queries.function.docvalues.FloatDocValues;
import org.apache.lucene.queries.function.docvalues.IntDocValues;
import org.apache.lucene.queries.function.docvalues.LongDocValues;
import org.apache.solr.schema.IndexSchema;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;

@SuppressWarnings("WeakerAccess")
public class DataFlowValueSource extends ValueSource {

    private DataFlowInstance instance;
    private IndexSchema indexSchema;
    private final Map<String, ValueSource> inputValueSources;

    public DataFlowValueSource(final DataFlowInstance instance, IndexSchema indexSchema,
            final Map<String, ValueSource> inputValueSources) {
        this.instance = instance;
        this.indexSchema = indexSchema;
        this.inputValueSources = inputValueSources;
    }

    @Override
    public FunctionValues getValues(final Map map, final LeafReaderContext leafReaderContext) {
        instance.forEachComponent(DocValueProviders.DocValueProvider.class, v -> {
            try {
                v.init(leafReaderContext, indexSchema);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        instance.forEachComponent((id, provider) -> {
            if (provider.getClass() == FunctionQueryValueProvider.class) {
                try {
                    FunctionQueryValueProvider fqValueProvider = (FunctionQueryValueProvider) provider;
                    fqValueProvider.init(inputValueSources.get(id), map, leafReaderContext);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        IntegerValue docIdValue = new IntegerValue(0);
        instance.setValue("docId", docIdValue);

        if (IntegerValue.class.getName().equals(instance.getOutputType())) {
            return new IntDocValues(this) {
                @Override
                public int intVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    return ((IntegerValue) instance.getOutput()).toInt();
                }
            };
        } else if (LongValue.class.getName().equals(instance.getOutputType())) {
            return new LongDocValues(this) {
                @Override
                public long longVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    return ((LongValue) instance.getOutput()).toLong();
                }
            };
        } else if (FloatValue.class.getName().equals(instance.getOutputType())) {
            return new FloatDocValues(this) {
                @Override
                public float floatVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    return ((FloatValue) instance.getOutput()).toFloat();
                }
            };
        } else if (DoubleValue.class.getName().equals(instance.getOutputType())) {
            return new DoubleDocValues(this) {
                @Override
                public double doubleVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    return ((DoubleValue) instance.getOutput()).toDouble();
                }
            };
        } else if (BooleanValue.class.getName().equals(instance.getOutputType())) {
            return new BoolDocValues(this) {
                @Override
                public boolean boolVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    return ((BooleanValue) instance.getOutput()).toBoolean();
                }
            };
        } else {
            return new FunctionValues() {
                @Override
                public Object objectVal(final int docId) {
                    docIdValue.setValue(docId);
                    instance.setValue("docId", docIdValue);
                    try {
                        instance.execute();
                    } catch (DataFlowExecutionException e) {
                        throw new RuntimeException(e);
                    }
                    Object outputValue = instance.getOutput();

                    if(outputValue != null) {
                        // Attempt to convert primitive value objects.
                        if (outputValue.getClass() == BooleanValue.class) {
                            outputValue = ((BooleanValue) outputValue).toBoolean();
                        } else if (outputValue.getClass() == IntegerValue.class) {
                            outputValue = ((IntegerValue) outputValue).toInt();
                        } else if (outputValue.getClass() == LongValue.class) {
                            outputValue = ((LongValue) outputValue).toLong();
                        } else if (outputValue.getClass() == FloatValue.class) {
                            outputValue = ((FloatValue) outputValue).toFloat();
                        } else if (outputValue.getClass() == DoubleValue.class) {
                            outputValue = ((DoubleValue) outputValue).toDouble();
                        }
                    }
                    return outputValue;
                }

                @Override
                public String toString(final int docId) {
                    Object obj = objectVal(docId);
                    return obj != null ? obj.toString() : "null";
                }
            };
        }
    }

    @Override
    public String description() {
        return "DataFlowValueSource instance=" + instance;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final DataFlowValueSource that = (DataFlowValueSource) o;
        return Objects.equals(instance, that.instance) &&
                Objects.equals(indexSchema, that.indexSchema) &&
                Objects.equals(inputValueSources, that.inputValueSources);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instance, indexSchema, inputValueSources);
    }
}

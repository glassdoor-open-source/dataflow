/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExpressionValueProviderTest.java
 */

package dataflow.core.component;

import dataflow.core.compiler.CompileException;
import java.util.Collections;
import java.util.Map;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.value.IntegerValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExpressionValueProviderTest {

    private DataFlowParser parser;
    private DataFlowEngine engine;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        this.parser = new DataFlowParser(engine);
    }

    @Test
    public void testExpression_noValueRef() throws Exception {
        testExprFlow("expr-flow-noValueRef.yaml", Collections.emptyMap(), 7);
    }

    @Test
    public void testExpression_singleValueRef() throws Exception {
        testExprFlow("expr-flow-singleValueRef.yaml",
                ImmutableMap.<String, Object>builder().put("testValue", 4).build(),
                4);
    }

    @Test
    public void testExpression_singleValueRef_withPropertyValueRef() throws Exception {
        testExprFlow("expr-flow-singleValueRef-withPropValueRef.yaml",
                Collections.emptyMap(),
                new IntegerValue(1));
    }

    @Test
    public void testExpression_multipleValueRefs() throws Exception {
        testExprFlow("expr-flow-multipleValueRefs.yaml",
                ImmutableMap.<String, Object>builder().put("testVal1", 3).put("testVal2", 7).build(),
                21);
    }

    @Test
    public void testExpression_singleValueRefInExpr() throws Exception {
        testExprFlow("expr-flow-singleValueRef-inExpr.yaml",
                ImmutableMap.<String, Object>builder().put("testValue", 4).build(),
                4);
    }

    @Test
    public void testExpression_singleValue_multipleInputs() throws Exception {
        testExprFlow("expr-flow-singleValueRef-multipleInputs.yaml",
                ImmutableMap.<String, Object>builder().put("input1", 4).put("input2", 6).build(),
                ImmutableMap.builder().put("in1", 4).put("in2", 6).build());
    }

    @Test
    public void testExpression_singleValue_withPropertyAndInput() throws Exception {
        testExprFlow("expr-flow-singleValueRef-withPropertyAndInput.yaml",
                Collections.emptyMap(),
                new IntegerValue(2));
    }

    @Test
    public void testExpression_nestedValueRef() throws Exception {
        testExprFlow("expr-flow-nestedValueRef.yaml",
                Collections.emptyMap(),
                ImmutableMap.builder().put("in", new IntegerValue(1)).build());
    }

    @Test
    public void testExpression_invalidExpr() throws Exception {
        testExprFlowWithError("expr-flow-invalidExpr.yaml",
                Collections.emptyMap(),
                e -> e.getCause().getCause() instanceof CompileException);
    }

    @Test
    public void testExpression_invalidValueRef() throws Exception {
        testExprFlowWithError("expr-flow-invalidValueRef.yaml",
                Collections.emptyMap(),
                exception -> true);
    }

    @Test
    public void testExpression_invalidValueRef_noId() throws Exception {
        testExprFlowWithError("expr-flow-invalidValueRef-noId.yaml",
                Collections.emptyMap(),
                exception -> true);
    }

    private void testExprFlowWithError(String filename, Map<String, Object> input, Predicate<Exception> exceptionPredicate) {
        try {
            testExprFlow(filename, input, null);
            fail("Expected exception was not thrown");
        } catch (Exception e) {
            e.printStackTrace();
            // Expected.
        }
    }

    private void testExprFlow(String filename, Map<String, Object> input, Object expectedOutput) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        try(DataFlowInstance instance = engine.newDataFlowInstance(config.getId())) {
            input.forEach(instance::setValue);
            instance.execute();
            assertEquals(expectedOutput, instance.getOutput());
        }
    }
}
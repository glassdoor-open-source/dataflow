/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListItemValueProviderUTest.java
 */

package dataflow.core.component.collection;

import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ListItemValueProviderUTest {

    private LinkedList<Object> testLinkedList;
    private ArrayList<Object> testArrayList;

    @Before
    public void setUp() {
        testLinkedList = new LinkedList<>();
        testLinkedList.add("a");
        testLinkedList.add("b");
        testLinkedList.add("c");

        testArrayList = new ArrayList<>();
        testArrayList.add("x");
        testArrayList.add("y");
        testArrayList.add("z");
    }

    @Test
    public void getValue_linkedList() {
        ListItemValueProvider valueProvider = new ListItemValueProvider(0);
        assertEquals("a", valueProvider.getValue(testLinkedList));
        valueProvider = new ListItemValueProvider(1);
        assertEquals("b", valueProvider.getValue(testLinkedList));
        valueProvider = new ListItemValueProvider(2);
        assertEquals("c", valueProvider.getValue(testLinkedList));
    }

    @Test
    public void getValue_linkedList_invalidIndex() {
        ListItemValueProvider valueProvider = new ListItemValueProvider(3);
        try {
            valueProvider.getValue(testLinkedList);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Invalid index: 3, list size: 3", ex.getMessage());
        }
    }

    @Test
    public void getValue_arrayList() {
        ListItemValueProvider valueProvider = new ListItemValueProvider(0);
        assertEquals("x", valueProvider.getValue(testArrayList));
        valueProvider = new ListItemValueProvider(1);
        assertEquals("y", valueProvider.getValue(testArrayList));
        valueProvider = new ListItemValueProvider(2);
        assertEquals("z", valueProvider.getValue(testArrayList));
    }

    @Test
    public void getValue_arrayList_invalidIndex() {
        ListItemValueProvider valueProvider = new ListItemValueProvider(3);
        try {
            valueProvider.getValue(testArrayList);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Invalid index: 3, list size: 3", ex.getMessage());
        }
    }
}
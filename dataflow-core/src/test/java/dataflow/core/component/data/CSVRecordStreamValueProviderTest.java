/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CSVRecordStreamValueProviderTest.java
 */

package dataflow.core.component.data;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;

import static org.junit.Assert.assertEquals;

public class CSVRecordStreamValueProviderTest {

    private DataFlowParser parser;
    private DataFlowEngine engine;

    @Before
    public void setUp() {
        this.engine = new DataFlowEngine();
        this.parser = new DataFlowParser(engine);
    }

    @Test
    public void testFlow() throws Exception {
        Stream<Map<String, Object>> resultStream = (Stream<Map<String, Object>>) testFlow("CSVRecordStreamValueProvider-flow1.yaml", Collections.emptyMap());
        List<Map<String, Object>> results = resultStream.collect(Collectors.toList());
        assertEquals(500, results.size());
        assertEquals("avg", results.get(0).get("field"));
        assertEquals("jobTitleHashCode", results.get(0).get("valueType"));
        assertEquals(839943242, results.get(0).get("value"));
        assertEquals(-0.005225271f, results.get(0).get("weight"));
        float[] expectedFactors = new float[]{0.030774692f, 0.030448996f, 0.029739283f, 0.02930015f, 0.02985974f,
            0.033203162f, 0.030737665f, 0.032454584f};
        for(int idx = 0; idx < expectedFactors.length; idx++) {
            assertEquals(expectedFactors[idx], ((List) results.get(0).get("factors")).get(idx));
        }
    }

    private Object testFlow(String filename, Map<String, Object> input) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        try(DataFlowInstance instance = engine.newDataFlowInstance(config.getId())) {
            input.forEach(instance::setValue);
            instance.execute();
            return instance.getOutput();
        }
    }
}
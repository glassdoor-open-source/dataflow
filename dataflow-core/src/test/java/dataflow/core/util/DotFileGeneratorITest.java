/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DotFileGeneratorITest.java
 */

package dataflow.core.util;

import org.junit.Test;

import dataflow.core.engine.DataFlowEngine;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;

import static org.junit.Assert.*;

public class DotFileGeneratorITest {

    @Test
    public void generateDOTFile() throws DataFlowParseException {
        DataFlowParser parser = new DataFlowParser(new DataFlowEngine());
        DataFlowConfig config = parser.parse(
                ClassLoader.getSystemResourceAsStream("dataflow/core/parser/test-flow-2.yaml"));
        assertEquals("digraph test_2 {\n" +
                "    goc_feature_fallback -> goc_feature_binned[ label=\"value\" ];\n" +
                "    _list0 -> goc_feature_fallback[ label=\"fallbackValues\" ];\n" +
                "    goc_feature -> goc_feature_fallback[ label=\"value\" ];\n" +
                "    _ConstValueProvider0 -> _list0[ label=\"\" ];\n" +
                "    _const0 -> goc_feature[ label=\"valueType\" ];\n" +
                "    _const1 -> goc_feature[ label=\"value\" ];\n" +
                "    goc_feature_fallback [label=< <b>goc_feature_fallback</b><br/><i>FallbackValueProvider</i> >];\n" +
                "    _const1 [label=< <b>1002</b><br/><i>ConstValueProvider</i> >];\n" +
                "    _const0 [label=< <b>GOC</b><br/><i>ConstValueProvider</i> >];\n" +
                "    goc_feature_binned [shape=diamond, label=< <b>goc_feature_binned</b><br/><i>BinnedValueProvider</i> >];\n" +
                "    goc_feature [label=< <b>goc_feature</b><br/><i>MemoryTableValueProvider</i> >];\n" +
                "    _list0 [label=< <i>ListValueProvider</i> >];\n" +
                "    _ConstValueProvider0 [label=< <b>5</b><br/><i>ConstValueProvider</i> >];\n" +
                "}\n", DotFileGenerator.generateDOTFile(config));
    }
}
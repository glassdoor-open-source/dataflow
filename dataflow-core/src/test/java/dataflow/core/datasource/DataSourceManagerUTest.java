/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceManagerUTest.java
 */

package dataflow.core.datasource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dataflow.test.datasource.TestDataSource;

import static org.junit.Assert.*;

public class DataSourceManagerUTest {

    private static class TestDataSourceEventListener implements DataSourceEventListener<TestDataSource> {

        private List<TestDataSource> added = new ArrayList<>();
        private List<TestDataSource> changed = new ArrayList<>();
        private List<TestDataSource> removed = new ArrayList<>();
        private List<Exception> errors = new ArrayList<>();

        @Override
        public void onAdd(final TestDataSource dataSource) {
            added.add(dataSource);
        }

        @Override
        public void onChange(final TestDataSource dataSource) {
            changed.add(dataSource);
        }

        @Override
        public void onRemove(final TestDataSource dataSource) {
            removed.add(dataSource);
        }

        @Override
        public void onError(final Exception error) {
            errors.add(error);
        }
    }

    private DataSourceManager<TestDataSource> manager;
    private TestDataSourceEventListener listener;

    @Before
    public void setUp() {
        manager = new DataSourceManager<>();
        listener = new TestDataSourceEventListener();
        manager.addEventListener(listener);
    }

    @After
    public void cleanUp() throws Exception {
        manager.close();
    }

    @Test
    public void testRefresh_singleDataSource() throws Exception {
        TestDataSource dataSource1 = new TestDataSource("/test/1", "one");

        DataSourceManager<TestDataSource>.DataSourceSupplierRegistration registration =
                manager.register(dataSource1, 1);

        assertEquals(1, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        dataSource1.setChanged(true);

        Thread.sleep(3000);
        assertEquals(1, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        manager.unregister(registration);

        Thread.sleep(3000);
        assertEquals(1, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(1, listener.removed.size());
        assertEquals(0, listener.errors.size());

        assertEquals(0, manager.getRegisteredDataSourceCount());
    }

    @Test
    public void testRefresh_singleDataSource_hasChangedError() throws Exception {
        AtomicBoolean throwException = new AtomicBoolean(false);
        TestDataSource dataSource1 = new TestDataSource("/test/1", "one") {
            @Override
            public boolean hasChanged() {
                if(throwException.get()) {
                    throw new RuntimeException();
                }
                return false;
            }
        };

        DataSourceManager<TestDataSource>.DataSourceSupplierRegistration registration =
                manager.register(dataSource1, 1);

        assertEquals(1, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        throwException.set(true);

        Thread.sleep(3000);
        assertEquals(1, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertTrue( listener.errors.size() > 0);

        manager.unregister(registration);

        assertEquals(0, manager.getRegisteredDataSourceCount());
    }

    @Test
    public void testRefresh_multipleDataSources() throws Exception {
        TestDataSource dataSource1 = new TestDataSource("/test/1", "one");
        TestDataSource dataSource2 = new TestDataSource("/test/2", "two");

        List<TestDataSource> providedDataSources = new ArrayList<>();
        manager.register(() -> providedDataSources, 1);

        assertEquals(0, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources.add(dataSource1);
        providedDataSources.add(dataSource2);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        dataSource1.setChanged(true);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources.remove(0);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(1, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources.clear();

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(2, listener.removed.size());
        assertEquals(0, listener.errors.size());

        assertEquals(0, manager.getRegisteredDataSourceCount());
    }

    @Test
    public void testRefresh_sameDataSourceMultipleProviders() throws Exception {
        TestDataSource dataSource1 = new TestDataSource("/test/1", "one");
        TestDataSource dataSource2 = new TestDataSource("/test/2", "two");

        List<TestDataSource> providedDataSources1 = new ArrayList<>();
        List<TestDataSource> providedDataSources2 = new ArrayList<>();
        manager.register(() -> providedDataSources1, 1);
        manager.register(() -> providedDataSources2, 1);

        assertEquals(0, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources1.add(dataSource1);
        providedDataSources2.add(dataSource1);
        providedDataSources2.add(dataSource2);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        dataSource1.setChanged(true);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources1.clear();

        // No datasources are removed because provider2 is still providing datasource1.

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources2.remove(dataSource1);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(1, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources2.remove(dataSource2);

        Thread.sleep(3000);
        assertEquals(2, listener.added.size());
        assertEquals(1, listener.changed.size());
        assertEquals(2, listener.removed.size());
        assertEquals(0, listener.errors.size());

        assertEquals(0, manager.getRegisteredDataSourceCount());
    }

    @Test
    public void testRefresh_sameDataSourceMultipleProviders_differentRefreshIntervals() throws Exception {
        TestDataSource dataSource1 = new TestDataSource("/test/1", "one");

        List<TestDataSource> providedDataSources1 = new ArrayList<>();
        List<TestDataSource> providedDataSources2 = new ArrayList<>();
        manager.register(() -> providedDataSources1, 2);
        providedDataSources1.add(dataSource1);

        Thread.sleep(5000);
        manager.register(() -> providedDataSources2, 1);

        assertEquals(1, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources2.add(dataSource1);

        Thread.sleep(5000);
        assertEquals(1, listener.added.size());
        assertEquals(0, listener.changed.size());
        assertEquals(0, listener.removed.size());
        assertEquals(0, listener.errors.size());

        providedDataSources1.clear();
        providedDataSources2.clear();

        Thread.sleep(3000);
        assertEquals(1, listener.added.size());
        assertEquals(1, listener.removed.size());
        assertEquals(0, listener.errors.size());

        assertEquals(0, manager.getRegisteredDataSourceCount());
    }
}
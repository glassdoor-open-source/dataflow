/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTest.java
 */

package dataflow.core.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.data.DataRecord;
import dataflow.core.data.DataRecords;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;

public class DataFlowTest {

    private DataFlowEngine engine;
    private DataFlowParser parser;
    private DataFlowEngine.DependencyInjector dependencyInjector;

    @Before
    public void setUp() {
        this.dependencyInjector = new SimpleDependencyInjector("myTestVal");
        this.engine = new DataFlowEngine(dependencyInjector);
        this.parser = new DataFlowParser(engine);

        DataFlowTestExtension.register(engine);
    }

    @Test
    public void test_singleValueProvider() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-singleValueProvider.yaml");
        instance.execute();
        assertEquals(1L, ((LongValue) instance.getOutput()).toLong());
    }

    @Test
    public void test_multipleInputValues() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-multipleInputValues.yaml");
        instance.setValue("in1", 35);
        instance.execute();
        assertEquals(57, ((IntegerValue) instance.getOutput()).toInt());
    }

    @Test
    public void test_placeholderString() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-placeholderStringProperty.yaml");
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A_%_@B_%_@C", instance.getOutput());
    }

    @Test
    public void test_placeholderString_multipleValueRefs() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-placeholderString-multipleValueRefs.yaml");
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A%_@B%_@C", instance.getOutput());
    }

    @Test
    public void test_placeholderStringInInput() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-placeholderStringInInput.yaml");
        instance.setValue("in1", ",");
        instance.setValue("in2", "A");
        instance.setValue("in3", "B");
        instance.execute();
        assertEquals("A_test2,test3_B", instance.getOutput());
    }

    @Test
    public void test_buildMap() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-buildMap.yaml");
        instance.setValue("in1", "X");
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertEquals(3, outputMap.size());
        assertEquals("X", outputMap.get("val1"));
        assertEquals("someValue2", outputMap.get("val2"));
        assertEquals("X,Y", outputMap.get("val3"));
    }

    @Test
    public void test_mapInput() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-mapInput.yaml");
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertNotNull(outputMap);
    }

    @Test
    public void test_mapEntry() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-mapEntry.yaml");
        instance.setValue("in1", "A");
        instance.setValue("in2", ImmutableMap.of("value", "B"));
        instance.execute();
        assertEquals("B", instance.getOutput());
    }

    @Test
    public void test_mapInvalidEntry() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-mapInvalidEntry.yaml");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (DataFlowExecutionException ex) {
            assertEquals("java.lang.IllegalArgumentException: Map must have an entry with key in2: {v2=B, v1=A}", ex.getMessage());
        }
    }

    @Test
    public void test_incremental() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-incremental.yaml");

        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in1", "A");
        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in2", "B");
        instance.execute();
        // Inputs 1 and 2 are set so the value of provider1 is available, but input 3 is not set so provider 2 value
        // is not available.
        assertEquals("A,B", instance.getValue("provider1"));
        assertNull(instance.getOutput());

        // All inputs are available so the value of provider2 is available.
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A,B", instance.getValue("provider1"));
        assertEquals("A,B,C", instance.getOutput());

        // Update the value of input 2. this should cause provider1 and provider2 values to be recomputed.
        instance.setValue("in2", "X");
        instance.execute();
        assertEquals("A,X", instance.getValue("provider1"));
        assertEquals("A,X,C", instance.getOutput());
    }

    @Test
    public void test_asyncValueProvider1() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncValueProvider1.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
    }

    @Test
    public void test_asyncValueProvider2() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncValueProvider2.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{in1={input=test}A, in2={input=test}B}C", instance.getOutput());
    }

    @Test
    public void test_syncValueProviderEventsEnabled() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-syncValueProviderEventsEnabled.yaml");
        Map<String, Long> executionTimeMap = new HashMap<>();
        instance.getEngine().getEventManager().addListener(new DataFlowEventListener() {
            @Override
            public void onDataFlowExecutionSuccess(final String dataFlowId, final String instanceId,
                    final long startTimeMillis,
                    final long endTimeMillis) {
                long runTimeMillis = endTimeMillis - startTimeMillis;
                System.out.println(dataFlowId + " " + instanceId + " " + runTimeMillis);
                assertTrue("Unexpected run time " + runTimeMillis, runTimeMillis >= 5000);
                super.onDataFlowExecutionSuccess(dataFlowId, instanceId, startTimeMillis, endTimeMillis);
            }

            @Override
            public void onComponentExecutionSuccess(final String dataFlowId, final String instanceId,
                    final String valueProviderId,
                    final long startTimeMillis, final long endTimeMillis) {
                executionTimeMap.put(valueProviderId, (endTimeMillis - startTimeMillis));
                System.out.println(valueProviderId + " " + (endTimeMillis - startTimeMillis) + " ms.");
            }
        });
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{in1={input=test}A, in2={input=test}B}C", instance.getOutput());
        assertEquals(3, executionTimeMap.size());
        assertTrue(executionTimeMap.get("provider1") >= 1000);
        assertTrue(executionTimeMap.get("provider2") >= 1000);
        assertTrue(executionTimeMap.get("provider3") >= 1000);
    }

    @Test
    public void test_asyncValueProviderEventsEnabled() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncValueProviderEventsEnabled.yaml");
        Map<String, Long> executionTimeMap = new HashMap<>();
        instance.getEngine().getEventManager().addListener(new DataFlowEventListener() {
            @Override
            public void onDataFlowExecutionSuccess(final String dataFlowId, final String instanceId,
                    final long startTimeMillis,
                    final long endTimeMillis) {
                long runTimeMillis = endTimeMillis - startTimeMillis;
                System.out.println(dataFlowId + " " + instanceId + " " + runTimeMillis);
                assertTrue("Unexpected run time " + runTimeMillis, runTimeMillis >= 2000);
                super.onDataFlowExecutionSuccess(dataFlowId, instanceId, startTimeMillis, endTimeMillis);
            }

            @Override
            public void onComponentExecutionSuccess(final String dataFlowId, final String instanceId,
                    final String valueProviderId,
                    final long startTimeMillis, final long endTimeMillis) {
                executionTimeMap.put(valueProviderId, (endTimeMillis - startTimeMillis));
                System.out.println(valueProviderId + " " + (endTimeMillis - startTimeMillis) + " ms.");
            }
        });
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{in1={input=test}A, in2={input=test}B}C", instance.getOutput());
        assertEquals(3, executionTimeMap.size());
        assertTrue(executionTimeMap.get("provider1") >= 1000);
        assertTrue(executionTimeMap.get("provider2") >= 1000);
        assertTrue(executionTimeMap.get("provider3") >= 1000);
    }

    @Test
    public void test_asyncThreadValueProvider1() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncThreadValueProvider1.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
    }

    @Test
    public void test_asyncThreadValueProvider2() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncThreadValueProvider2.yaml");
        instance.setValue("in", "test");
        instance.execute();
        Stopwatch stopwatch = Stopwatch.createStarted();
        assertEquals("{in1={input=test}A, in2={input=test}B, in3={input=test}C, in4={input=test}D}E", instance.getOutput());

        // If the four inputs execute concurrently then this test should complete in about 10 seconds.
        assertTrue(stopwatch.elapsed(TimeUnit.SECONDS) < 15);
    }

    @Test
    public void test_syncValueProviderAfterAsync() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-syncValueProviderAfterAsync.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input=test}A:{input=test}B", instance.getOutput());
    }

    @Test
    public void test_errorNoFallback() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-errorNoFallback.yaml");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
    }

    @Test
    public void test_syncErrorFallback() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-syncErrorFallback.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_const() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncErrorFallback_const.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_sync() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncErrorFallback_sync.yaml");
        instance.setValue("in1", "test");
        instance.execute();
        assertEquals("test", instance.getOutput());
    }

    @Test
    public void test_asyncErrorNoFallback() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncErrorNoFallback.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals(null, instance.getOutput());
    }

    @Test
    public void test_syncMissingValueFallback() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-syncMissingValueFallback.yaml");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_joinList() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-joinList.yaml");
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        assertEquals("X:Y:Z", instance.getOutput());
    }

    @Test
    public void test_transformDataRecords() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-transformDataRecords.yaml");
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("count", 5);
        records.add(record);
        instance.setValue("in", records);
        instance.execute();
        DataRecords output = (DataRecords) instance.getOutput();
        assertEquals(1, output.size());
        assertEquals(2, output.get(0).size());
        assertEquals(6, output.get(0).get("count"));
        assertEquals("abc", output.get(0).get("test"));
        assertEquals(1, records.get(0).size());
        assertEquals(ImmutableList.of("count", "test"), output.getColumnNames());
    }

    @Test
    public void test_dynamicProperty() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-dynamicProperty.yaml");
        instance.setValue("in3", "X");
        instance.setValue("in4", "Y");
        instance.execute();
        assertEquals("BA:XY", instance.getOutput());
    }

    @Test
    public void test_customProperties() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-customProperties.yaml");
        instance.execute();
        assertEquals("EDCBA", instance.getOutput());
    }

    @Test
    public void test_dynamicProvidedProperty() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-dynamicProvidedProperty.yaml");
        printSource();
        instance.setValue("separator", "@");
        instance.setValue("in1", "X");
        instance.setValue("in2", "Y");
        instance.execute();
        assertEquals("X@Y", instance.getOutput());
    }

    @Test
    public void test_missingInput() throws Exception {
        try {
            registerAndGetInstance("TestFlow-missingInput.yaml");
            fail("Expected exception was not thrown");
        } catch (DataFlowConfigurationException ex) {
            // Expected.
            assertEquals("Unable to register dataflow flow", ex.getMessage());
            assertEquals("Input values not defined for component provider1", ex.getCause().getMessage());
        }
    }

    @Test
    public void test_intConstToStringConversion() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-intConstToStringConversion.yaml");
        instance.execute();
        assertEquals("54321", instance.getOutput());
    }

    @Test
    public void test_intToStringConversion() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-intToStringConversion.yaml");
        instance.setValue("in1", 12345);
        instance.setValue("in2", 56789);
        instance.execute();
        assertEquals("98765", instance.getOutput());
    }

    @Test
    public void test_dependencyInjection() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-dependencyInjection.yaml");
        instance.execute();
        assertEquals("myTestVal", instance.getOutput());
    }

    @Test
    public void test_constOutput() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-constOutput.yaml");
        instance.execute();
        assertEquals("result", instance.getOutput());
    }

    @Test
    public void test_asyncError_valueCleared() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-asyncErrorValueCleared.yaml");
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
    }

    @Test
    public void test_syncError_valueCleared() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-syncErrorValueCleared.yaml");
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
    }

    @Test
    public void test_optionalInput_missing() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-optionalInputMissing.yaml");
        instance.execute();
        assertEquals("EMPTY", instance.getOutput());
    }

    @Test
    public void test_optionalInput_present() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("TestFlow-optionalInputPresent.yaml");
        instance.execute();
        assertEquals("test", instance.getOutput());
    }

    @Test
    public void test_sink() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = registerAndGetInstance("TestFlow-sink.yaml");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals(ImmutableMap.of("v1", "A", "v2", "B"), instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("test", list.get(0));
    }

    @Test
    public void test_sinkWithOutputAsDependency() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = registerAndGetInstance("TestFlow-sinkWithOutputAsDependency.yaml");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals("A:B", instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("A:B", list.get(0));
    }

    @Test
    public void test_sink_sinkPropertyNotSet() {
        try {
            registerAndGetInstance("TestFlow-sink-propNotSet.yaml");
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("[_TestSinkValueProvider0] components are not used and are not abstract", ex.getCause().getMessage());
        }
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream(filename));
        engine.registerDataFlow(config);
        printSource();
        return engine.newDataFlowInstance("flow");
    }

    private void printSource() {
        System.out.println(engine.getSource("flow"));
    }
}

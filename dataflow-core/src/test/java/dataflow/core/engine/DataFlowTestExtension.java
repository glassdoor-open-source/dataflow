/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTestExtension.java
 */

package dataflow.core.engine;

import dataflow.test.provider.*;

public class DataFlowTestExtension {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(CounterValueProviderMetadata.INSTANCE, new CounterValueProviderBuilder(engine));
        engine.registerComponent(TestAsyncValueProviderMetadata.INSTANCE,
                new TestAsyncValueProviderBuilder(engine));
        engine.registerComponent(IntegerMaxValueProviderMetadata.INSTANCE,
                new IntegerMaxValueProviderBuilder(engine));
        engine.registerComponent(FailingValueProviderMetadata.INSTANCE, new FailingValueProviderBuilder(engine));
        engine.registerComponent(FailingAsyncValueProviderMetadata.INSTANCE, new FailingAsyncValueProviderBuilder(engine));
        engine.registerComponent(NullValueProviderMetadata.INSTANCE, new NullValueProviderBuilder(engine));
        engine.registerComponent(NullAsyncValueProviderMetadata.INSTANCE, new NullAsyncValueProviderBuilder(engine));
        engine.registerComponent(OptionalInputValueProviderMetadata.INSTANCE, new OptionalInputValueProviderBuilder(engine));
        engine.registerComponent(TestAsyncThreadValueProviderMetadata.INSTANCE,
                new TestAsyncThreadValueProviderBuilder(engine));
        engine.registerComponent(SyncWithWaitValueProviderMetadata.INSTANCE,
                new SyncWithWaitValueProviderBuilder(engine));
        engine.registerComponent(TestInjectionValueProviderMetadata.INSTANCE,
                new TestInjectionValueProviderBuilder(engine));
        engine.registerComponent(TestSinkValueProviderMetadata.INSTANCE, new TestSinkValueProviderBuilder(engine));
        engine.registerComponent(StringJoinValueProviderMetadata.INSTANCE, new StringJoinValueProviderBuilder(engine));
        engine.registerComponent(TransformedStringValueProviderMetadata.INSTANCE, new TransformedStringValueProviderBuilder(engine));
        engine.registerComponent(TestPreloadingValueProviderMetadata.INSTANCE, new TestPreloadingValueProviderBuilder(engine));
        engine.registerPropertyObjectBuilder(MaxLengthStringTransformer.class, new MaxLengthStringTransformerBuilder(engine));
        engine.registerPropertyObjectBuilder(ToUppercaseStringTransformer.class, new ToUppercaseStringTransformerBuilder(engine));
        engine.registerPropertyObjectBuilder(ReverseStringTransformer.class, new ReverseStringTransformerBuilder(engine));
        engine.registerComponent(TestRetryValueProviderMetadata.INSTANCE, new TestRetryValueProviderBuilder(engine));
    }
}

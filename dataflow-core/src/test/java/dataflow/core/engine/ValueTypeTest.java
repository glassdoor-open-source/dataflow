/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueTypeTest.java
 */

package dataflow.core.engine;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValueTypeTest {

    @Test
    public void fromString_noGenericTypes() {
        ValueType type = ValueType.fromString("java.util.String");
        assertEquals("java.util.String", type.toString());
    }

    @Test
    public void fromString_singleGenericType() {
        ValueType type = ValueType.fromString("List<Integer>");
        assertEquals("List<Integer>", type.toString());
    }

    @Test
    public void fromString_multipleGenericTypes() {
        ValueType type = ValueType.fromString("Map<String, Float>");
        assertEquals("Map<String,Float>", type.toString());
    }

    @Test
    public void fromString_nestedGenericTypes() {
        ValueType type = ValueType.fromString("java.util.Map<String, List<Double>>");
        assertEquals("java.util.Map<String,List<Double>>", type.toString());
    }

    @Test
    public void fromString_multipleNestedGenericTypes() {
        ValueType type = ValueType.fromString("List<String, Map<Integer, Double>>");
        assertEquals("List<String,Map<Integer,Double>>", type.toString());
    }
}
/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  GraphUtilTest.java
 */

package dataflow.core.engine;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import com.google.common.collect.ImmutableList;

import static org.junit.Assert.*;

/**
 * Unit test for {@link GraphUtil}.
 */
public class GraphUtilTest {

    private static class TestNode {

        String id;
        List<TestNode> dependencies = new ArrayList<>();

        public TestNode(String id) {
            this.id = id;
        }

        public String toString() {
            return id;
        }
    }

    @Test
    public void testTopologicalSort() {
        TestNode a = new TestNode("a");
        TestNode b = new TestNode("b");
        TestNode c = new TestNode("c");
        TestNode d = new TestNode("d");

        d.dependencies.add(c);
        d.dependencies.add(b);
        d.dependencies.add(a);
        c.dependencies.add(b);
        b.dependencies.add(a);
        List<TestNode> sortedNodes = GraphUtil.topologicalSort(d, n -> n.dependencies);
        assertEquals(ImmutableList.of(a, b, c, d), sortedNodes);
    }

    @Test
    public void testTopologicalSort_cycle() {
        TestNode a = new TestNode("a");
        TestNode b = new TestNode("b");
        TestNode c = new TestNode("c");
        TestNode d = new TestNode("d");

        d.dependencies.add(c);
        c.dependencies.add(b);
        b.dependencies.add(a);
        a.dependencies.add(d);
        try {
            GraphUtil.topologicalSort(d, n -> n.dependencies);
            fail("Expected exception not thrown");
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }
}
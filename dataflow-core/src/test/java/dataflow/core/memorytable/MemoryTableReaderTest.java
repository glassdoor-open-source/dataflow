/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableReaderTest.java
 */

package dataflow.core.memorytable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.datasource.ClassPathFileDataSource;
import dataflow.core.datasource.DataSource;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.component.data.RecordSchema;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MemoryTableReaderTest {

    @Test
    public void testRead() throws IOException {
        List<String> columnNames = ImmutableList.of("field", "valueType", "value", "weight", "factors");
        Map<String, String> columnValueTypeMap = ImmutableMap.<String, String>builder()
                .put("field", "string")
                .put("valueType", "string")
                .put("value", "long")
                .put("weight", "float")
                .put("factors", "double[]")
                .build();
        List<String> keyColumns = ImmutableList.of("field", "valueType", "value");
        DataSource source = new ClassPathFileDataSource("dataflow/core/memorytable/ffm.tsv");
        CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter('\t');

        MemoryTableReader reader = new MemoryTableReader(new DataFlowEngine());
        MemoryTable memoryTable = reader.read(
                new MemoryTableDescriptor(new RecordSchema(columnNames, columnValueTypeMap), keyColumns, source, -1, csvFormat));
        assertTrue(memoryTable.size() > 0);

        List<Map<String, Object>> rows = memoryTable.get(new MemoryTableKey(new Object[]{
                "avg",
                "jobTitleHashCode",
                984273382L}));
        assertEquals(1, rows.size());
        Map<String, Object> row = rows.get(0);
        assertEquals(-0.0029964542, ((Float) row.get("weight")), .001);
        assertArrayEquals(
                new double[]{0.03105285, 0.033547193, 0.03260833, 0.028964736, 0.03176584, 0.031317335, 0.033263512, 0.03203904},
                (double[]) row.get("factors"), .001);
    }
}
/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowParserTest.java
 */

package dataflow.core.parser;

import org.junit.Before;
import org.junit.Test;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowParseException;

import static org.junit.Assert.*;

public class DataFlowParserTest {

    private DataFlowParser parser;

    @Before
    public void setUp() {
        this.parser = new DataFlowParser(new DataFlowEngine());
    }

    @Test
    public void testParser_flow1() throws DataFlowConfigurationException, DataFlowParseException {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream("test-flow-1.yaml"));
        assertEquals("DataFlowConfig{id='null', components={pclick=ComponentConfig {id='pclick', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[pclick_binned], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, pclick_binned=ComponentConfig {id='pclick_binned', type='BinnedValueProvider', extendsId='null', properties={bins=[0, 0.6033935, 0.6284179, 0.64703363, 0.66284174, 0.6801757, 0.70019525, 0.7250976, 0.75732416, 0.8002929]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}, output=ComponentConfig {id='pclick_binned', type='BinnedValueProvider', extendsId='null', properties={bins=[0, 0.6033935, 0.6284179, 0.64703363, 0.66284174, 0.6801757, 0.70019525, 0.7250976, 0.75732416, 0.8002929]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}", config.toString());
    }

    @Test
    public void testParser_flow2() throws DataFlowConfigurationException, DataFlowParseException {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream("test-flow-2.yaml"));
        assertEquals("DataFlowConfig{id='test_2', components={goc_feature_fallback=ComponentConfig {id='goc_feature_fallback', type='FallbackValueProvider', extendsId='null', properties={}, propertyValueProviders=[], input=[fallbackValues, value], output=[goc_feature_binned], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _const1=ComponentConfig {id='_const1', type='ConstValueProvider', extendsId='null', properties={value=1002}, propertyValueProviders=[], input=[], output=[goc_feature], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, ffm_feature=ComponentConfig {id='ffm_feature', type='MemoryTableValueProvider', extendsId='null', properties={schema=valueType:string, value:integer, field:string, weight:float, factors:float[], validators=[{minRows=100000}], keyColumns=[valueType, value], outputColumns=[field, weight, factors], checkIntervalSecs=300, outputMode=SINGLE_ROW, source={filename=ffm.tsv}, sourceFormat=tsv, lazyLoad=false}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=true, internal=false, eventsEnabled=false}, _const0=ComponentConfig {id='_const0', type='ConstValueProvider', extendsId='null', properties={value=GOC}, propertyValueProviders=[], input=[], output=[goc_feature], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, goc_feature_binned=ComponentConfig {id='goc_feature_binned', type='BinnedValueProvider', extendsId='null', properties={bins=[0.25, 0.5, 0.75]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, goc_feature=ComponentConfig {id='goc_feature', type='MemoryTableValueProvider', extendsId='ffm_feature', properties={schema=valueType:string, value:integer, field:string, weight:float, factors:float[], validators=[{minRows=100000}], keyColumns=[valueType, value], outputColumns=[field, weight, factors], checkIntervalSecs=300, outputMode=SINGLE_ROW, source={filename=ffm.tsv}, lazyLoad=true, sourceFormat=tsv}, propertyValueProviders=[], input=[valueType, value], output=[goc_feature_fallback], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _list0=ComponentConfig {id='_list0', type='ListValueProvider', extendsId='null', properties={itemKeys=[0]}, propertyValueProviders=[], input=[0], output=[goc_feature_fallback], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, _ConstValueProvider0=ComponentConfig {id='_ConstValueProvider0', type='ConstValueProvider', extendsId='null', properties={value=5}, propertyValueProviders=[], input=[], output=[_list0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}}, output=ComponentConfig {id='goc_feature_binned', type='BinnedValueProvider', extendsId='null', properties={bins=[0.25, 0.5, 0.75]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}", config.toString());
    }
}
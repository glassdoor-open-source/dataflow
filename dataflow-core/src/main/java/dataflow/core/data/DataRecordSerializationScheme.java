/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordSerializationScheme.java
 */

package dataflow.core.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface DataRecordSerializationScheme {

    DataRecordReader createReader(InputStream in) throws IOException;
    DataRecordWriter createWriter(OutputStream out) throws IOException;
}

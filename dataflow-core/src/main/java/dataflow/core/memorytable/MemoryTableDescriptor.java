/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableDescriptor.java
 */

package dataflow.core.memorytable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.csv.CSVFormat;
import dataflow.core.datasource.DataSource;
import dataflow.core.component.data.RecordSchema;

public class MemoryTableDescriptor implements DataSource {

    private List<String> columnNames;
    private Map<String, String> columnNameValueTypeMap;
    private List<String> keyColumns;
    private DataSource dataSource;
    private long refreshIntervalSeconds;
    private CSVFormat csvFormat;

    public MemoryTableDescriptor(final RecordSchema schema, final List<String> keyColumns,
            final DataSource dataSource, long refreshIntervalSeconds, final CSVFormat csvFormat) {
        this(schema.getColumnNames(), schema.getColumnValueTypes(), keyColumns, dataSource, refreshIntervalSeconds,
                csvFormat);
    }

    public MemoryTableDescriptor(final List<String> columnNames,
            final Map<String, String> columnNameValueTypeMap, final List<String> keyColumns,
            final DataSource dataSource, long refreshIntervalSeconds, final CSVFormat csvFormat) {
        this.columnNames = columnNames;
        this.columnNameValueTypeMap = columnNameValueTypeMap;
        this.keyColumns = keyColumns;
        this.dataSource = dataSource;
        this.refreshIntervalSeconds = refreshIntervalSeconds;
        this.csvFormat = csvFormat;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public Map<String, String> getColumnNameValueTypeMap() {
        return columnNameValueTypeMap;
    }

    public List<String> getKeyColumns() {
        return keyColumns;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public long getRefreshIntervalSeconds() {
        return refreshIntervalSeconds;
    }

    public CSVFormat getCsvFormat() {
        return csvFormat;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final MemoryTableDescriptor that = (MemoryTableDescriptor) o;
        return Objects.equals(columnNames, that.columnNames) &&
                Objects.equals(columnNameValueTypeMap, that.columnNameValueTypeMap) &&
                Objects.equals(keyColumns, that.keyColumns) &&
                Objects.equals(dataSource, that.dataSource) &&
                Objects.equals(csvFormat, that.csvFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnNames, columnNameValueTypeMap, keyColumns, dataSource, csvFormat);
    }

    @Override
    public String toString() {
        return "MemoryTableDescriptor{" +
                "columnNames=" + columnNames +
                ", columnNameValueTypeMap=" + columnNameValueTypeMap +
                ", keyColumns=" + keyColumns +
                ", dataSource=" + dataSource +
                ", refreshIntervalSeconds=" + refreshIntervalSeconds +
                ", csvFormat=" + csvFormat +
                '}';
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return dataSource.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return dataSource.getOutputStream();
    }

    @Override
    public List<DataSource> getChildren() throws IOException {
        return dataSource.getChildren();
    }

    @Override
    public DataSource getParent() throws IOException {
        return dataSource.getParent();
    }

    @Override
    public boolean hasChanged() throws IOException {
        return dataSource.hasChanged();
    }

    @Override
    public boolean exists() throws IOException {
        return dataSource.exists();
    }

    @Override
    public void delete() throws IOException {
        dataSource.delete();
    }

    @Override
    public String getPath() {
        return dataSource.getPath();
    }

    @Override
    public List<DataSource> getRoots() {
        return null;
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) throws IOException {
        return null;
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableValueProviderCodeGenerator.java
 */

package dataflow.core.memorytable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import dataflow.annotation.processor.IndentPrintWriter;
import dataflow.core.engine.codegen.CodeGenerationUtil;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.engine.codegen.DefaultComponentCodeGenerator;
import dataflow.core.parser.ComponentConfig;

import static dataflow.core.engine.codegen.CodeGenerationUtil.getValueReferenceExpression;
import static dataflow.core.engine.codegen.CodeGenerationUtil.isConstant;

public class MemoryTableValueProviderCodeGenerator extends DefaultComponentCodeGenerator {

    private static final String[] IMPORTS = new String[]{MemoryTableKey.class.getName()};

    public MemoryTableValueProviderCodeGenerator() {
        super(null);
    }

    @Override
    public Set<String> getImports() {
        Set<String> imports = new HashSet<>(Arrays.asList(IMPORTS));
        imports.addAll(super.getImports());
        return imports;
    }

    @Override
    public void writeFields(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        super.writeFields(out, context);
        out.printf("private Object[] %s_keyComponents;%n", context.getVarNamePrefix());
    }

    @Override
    public void writeConstructorStatements(final IndentPrintWriter out,
            final ComponentCodeGenerationContext context) {
        super.writeConstructorStatements(out, context);
        out.printf("this.%s_keyComponents = %s_component.getKey().getComponents();%n",
                context.getVarNamePrefix(), context.getVarNamePrefix());
        writeSetKeyComponents(out, CodeGenerationUtil::isConstant, context);
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        writeSetKeyComponents(out, cfg -> !isConstant(cfg), context);
        out.printf("%s_value = %s_component.getValue();%n", context.getVarNamePrefix(), context.getVarNamePrefix());
    }

    private void writeSetKeyComponents(IndentPrintWriter out, Predicate<ComponentConfig> inputConfigPredicate,
            ComponentCodeGenerationContext context) {
        DataFlowCodeGenerationContext dataFlowContext = context.getDataFlowContext();
        ComponentConfig config = context.getComponentConfig();
        List<String> keyColumns = (List<String>) config.getProperties().get("keyColumns");
        if(keyColumns == null) {
            throw new IllegalArgumentException("keyColumns not set for MemoryTableValueProvider " + config.getId());
        }
        for (int idx = 0; idx < keyColumns.size(); idx++) {
            String keyColumn = keyColumns.get(idx);
            ComponentConfig inputConfig = config.getInput().get(keyColumn);
            if(inputConfig != null && inputConfigPredicate.test(inputConfig)) {
                String valueExpr = getValueReferenceExpression(inputConfig, context);
                String fromType = dataFlowContext.getFieldType(inputConfig.getId());
                String toType = config.getInputTypes().get(keyColumn);
                if(toType != null) {
                    valueExpr = CodeGenerationUtil.valueConvertExpr(fromType, toType, valueExpr, dataFlowContext);
                }

                out.printf("%s_keyComponents[%d] = %s;%n", context.getVarNamePrefix(), idx, valueExpr);
            }
        }
    }
}

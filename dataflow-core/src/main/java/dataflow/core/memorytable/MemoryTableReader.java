/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableReader.java
 */

package dataflow.core.memorytable;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.ValueRegistry;
import dataflow.core.engine.ValueType;

public class MemoryTableReader {

    private DataFlowEngine engine;

    public MemoryTableReader(final DataFlowEngine engine) {
        this.engine = engine;
    }

    public MemoryTable read(final MemoryTableDescriptor descriptor) throws IOException {
        Map<MemoryTableKey, List<Map<String, Object>>> data = new HashMap<>();

        List<String> keyColumnsNameValues = descriptor.getKeyColumns().stream()
                .map(String::new)
                .collect(Collectors.toList());
        List<String> columnNameValues = descriptor.getColumnNames().stream()
                .map(String::new)
                .collect(Collectors.toList());

        Map<String, String> strings = new HashMap<>();
        Reader in = new InputStreamReader(descriptor.getDataSource().getInputStream());
        int columnCount = descriptor.getColumnNames().size();
        try (CSVParser parser = new CSVParser(in, descriptor.getCsvFormat())) {
            for (CSVRecord record : parser.getRecords()) {
                Map<String, Object> row = new HashMap<>(record.size());
                for (int idx = 0; idx < record.size() && idx < columnCount; idx++) {
                    String columnName = columnNameValues.get(idx);
                    String type = descriptor.getColumnNameValueTypeMap().get(columnName);

                    String strValue = record.get(idx);

                    if (type.equalsIgnoreCase(ValueRegistry.STRING_TYPE)) {
                        String existingString = strings.get(strValue);
                        if (existingString != null) {
                            strValue = existingString;
                        } else {
                            strings.put(strValue, strValue);
                        }
                    }
                    Object value = engine.convert(strValue, ValueRegistry.STRING_VALUE_TYPE, ValueType.fromString(type));
                    row.put(columnName, value);
                }

                Object[] keyComponents = new Object[keyColumnsNameValues.size()];
                for (int idx = 0; idx < keyComponents.length; idx++) {
                    keyComponents[idx] = row.get(keyColumnsNameValues.get(idx));
                }
                MemoryTableKey key = new MemoryTableKey(keyComponents);
                data.computeIfAbsent(key, t -> new ArrayList<>()).add(row);
            }
        }

        return new MemoryTable(data);
    }
}

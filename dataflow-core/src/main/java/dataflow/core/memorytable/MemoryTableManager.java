/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableManager.java
 */

package dataflow.core.memorytable;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import dataflow.core.datasource.DataSourceEventListener;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.engine.DataFlowEngine;

public class MemoryTableManager implements AutoCloseable {

    // TODO(thorntonv): Integrate with DataRecord.

    private final Map<MemoryTableDescriptor, MemoryTable> descriptorTableMap = new ConcurrentHashMap<>();
    private final MemoryTableReader tableReader;
    private DataSourceManager<MemoryTableDescriptor> dataSourceManager = new DataSourceManager<>();

    public MemoryTableManager(DataFlowEngine engine) {
        this.tableReader = new MemoryTableReader(engine);

        init();
    }

    private void init() {
        dataSourceManager = new DataSourceManager<>();
        dataSourceManager.addEventListener(new DataSourceEventListener<MemoryTableDescriptor>() {
            @Override
            public void onAdd(final MemoryTableDescriptor descriptor) {
                onChange(descriptor);
            }

            @Override
            public void onChange(final MemoryTableDescriptor descriptor) {
                try {
                    descriptorTableMap.put(descriptor, tableReader.read(descriptor));
                } catch (IOException e) {
                    onError(e);
                }
            }

            @Override
            public void onRemove(final MemoryTableDescriptor descriptor) {
                descriptorTableMap.remove(descriptor);
            }

            @Override
            public void onError(final Exception error) {
                error.printStackTrace();
            }
        });
    }

    public synchronized void reset() throws Exception {
        descriptorTableMap.clear();
        if (dataSourceManager != null) {
            dataSourceManager.close();
        }
        init();
    }

    public synchronized DataSourceManager<MemoryTableDescriptor>.DataSourceSupplierRegistration register(
            Callable<List<MemoryTableDescriptor>> descriptorSupplier, long refreshIntervalSeconds) {
        return dataSourceManager.register(descriptorSupplier, refreshIntervalSeconds);
    }

    public synchronized DataSourceManager<MemoryTableDescriptor>.DataSourceSupplierRegistration register(
            MemoryTableDescriptor descriptor) {
        return register(() -> Collections.singletonList(descriptor), descriptor.getRefreshIntervalSeconds());
    }

    public MemoryTable get(MemoryTableDescriptor memoryTableDescriptor) {
        return descriptorTableMap.get(memoryTableDescriptor);
    }

    public synchronized void unregister(final DataSourceManager<MemoryTableDescriptor>.DataSourceSupplierRegistration supplierRegistration) {
        dataSourceManager.unregister(supplierRegistration);
    }

    @Override
    public synchronized void close() throws Exception {
        dataSourceManager.close();
    }

    public int getAllocatedMemoryTableDescriptorsCount() {
        return dataSourceManager.getRegisteredDataSourceCount();
    }
}

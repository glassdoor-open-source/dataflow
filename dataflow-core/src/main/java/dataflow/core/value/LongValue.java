/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LongValue.java
 */

package dataflow.core.value;

import java.util.Objects;

/**
 * A mutable wrapper for primitive long values.
 */
@SuppressWarnings("unused")
public class LongValue {

    public static final String TYPE = "long";

    private long value;
    private boolean set = false;

    public LongValue() {
    }

    public LongValue(long value) {
        setValue(value);
    }

    public String getType() {
        return TYPE;
    }

    public LongValue setValue(long value) {
        this.value = value;
        set = true;
        return this;
    }

    public LongValue setValue(Long value) {
        if (value != null) {
            setValue(value.longValue());
        } else {
            clear();
        }
        return this;
    }

    public LongValue setValue(LongValue value) {
        if (value != null) {
            this.value = value.value;
            this.set = value.set;
        } else {
            clear();
        }
        return this;
    }

    public long toLong() {
        return value;
    }

    public void fromLong(long value) {
        this.value = value;
        set = true;
    }

    public float toFloat() {
        return (float) value;
    }

    public double toDouble() {
        return (double) value;
    }

    public boolean toBoolean() {
        return value != 0;
    }

    public void fromString(final String str) {
        value = Long.parseLong(str);
        set = true;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public boolean isSet() {
        return set;
    }

    public void clear() {
        set = false;
        value = 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final LongValue longValue = (LongValue) o;
        return value == longValue.value &&
                set == longValue.set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, set);
    }
}

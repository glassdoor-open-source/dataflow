/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BooleanValue.java
 */

package dataflow.core.value;

import java.util.Objects;

/**
 * A mutable wrapper for primitive boolean values.
 */
@SuppressWarnings("unused")
public class BooleanValue {

    public static final String TYPE = "boolean";

    private boolean value;
    private boolean set = false;

    public BooleanValue() {
    }

    public BooleanValue(boolean value) {
        setValue(value);
    }

    public String getType() {
        return TYPE;
    }

    public BooleanValue setValue(boolean value) {
        this.value = value;
        set = true;
        return this;
    }

    public BooleanValue setValue(Boolean value) {
        if (value != null) {
            setValue(value.booleanValue());
        } else {
            clear();
        }
        return this;
    }

    public BooleanValue setValue(BooleanValue value) {
        if (value != null) {
            this.value = value.value;
            this.set = value.set;
        } else {
            clear();
        }
        return this;
    }

    public int toInt() {
        return value ? 1 : 0;
    }

    public void fromInt(int value) {
        this.value = value != 0;
        set = true;
    }

    public long toLong() {
        return value ? 1 : 0;
    }

    public void fromLong(long value) {
        this.value = value != 0;
        set = true;
    }

    public float toFloat() {
        return value ? 1.0f : 0.0f;
    }

    public void fromFloat(float value) {
        this.value = value != 0.0f;
        set = true;
    }

    public double toDouble() {
        return value ? 1.0 : 0.0;
    }

    public void fromDouble(double value) {
        this.value = value != 0.0d;
        set = true;
    }

    public boolean toBoolean() {
        return value;
    }

    public void fromBoolean(boolean value) {
        this.value = value;
        set = true;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public void fromString(final String str) {
        value = Boolean.parseBoolean(str);
        set = true;
    }

    public boolean isSet() {
        return set;
    }

    public void clear() {
        set = false;
        value = false;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final BooleanValue that = (BooleanValue) o;
        return value == that.value &&
                set == that.set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, set);
    }
}

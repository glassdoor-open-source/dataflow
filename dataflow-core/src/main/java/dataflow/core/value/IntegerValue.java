/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerValue.java
 */

package dataflow.core.value;

import java.util.Objects;

/**
 * A mutable wrapper for primitive integer values.
 */
@SuppressWarnings("unused")
public class IntegerValue {

    public static final String TYPE = "integer";

    private int value;
    private boolean set = false;

    public IntegerValue() {
    }

    public IntegerValue(int value) {
        setValue(value);
    }

    public String getType() {
        return TYPE;
    }

    public IntegerValue setValue(int value) {
        this.value = value;
        set = true;
        return this;
    }

    public IntegerValue setValue(Integer value) {
        if (value != null) {
            setValue(value.intValue());
        } else {
            clear();
        }
        return this;
    }

    public IntegerValue setValue(IntegerValue value) {
        if(value != null) {
            this.value = value.value;
            this.set = value.set;
        } else {
            clear();
        }
        return this;
    }

    public int toInt() {
        return value;
    }

    public void fromInt(final int value) {
        this.value = value;
        set = true;
    }

    public long toLong() {
        return value;
    }

    public float toFloat() {
        return (float) value;
    }

    public double toDouble() {
        return value;
    }

    public boolean toBoolean() {
        return value != 0;
    }

    public void fromString(final String str) {
        value = Integer.parseInt(str);
        set = true;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public boolean isSet() {
        return set;
    }

    public void clear() {
        set = false;
        value = 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final IntegerValue that = (IntegerValue) o;
        return value == that.value &&
                set == that.set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, set);
    }
}

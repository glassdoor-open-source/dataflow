/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AllUnorderedPairsValueProvider.java
 */

package dataflow.core.component.math;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.DataFlowConfigurable;

@DataFlowComponent
public class AllUnorderedPairsValueProvider {

    @DataFlowConfigurable
    public AllUnorderedPairsValueProvider() {}

    @OutputValue
    public Stream<List<Object>> getValue(@InputValue List<Object> list) {
        ArrayList<Object> arrayList;
        if (list instanceof ArrayList) {
            arrayList = (ArrayList<Object>) list;
        } else {
            arrayList = new ArrayList<>(list);
        }
        List<List<Object>> output = new ArrayList<>();
        for(int i = 0; i < arrayList.size(); i++) {
            Object item1 = arrayList.get(i);
            for(int j = i + 1; j < arrayList.size(); j++) {
                Object item2 = arrayList.get(j);
                List<Object> pair = new ArrayList<>();
                pair.add(item1);
                pair.add(item2);
                output.add(pair);
            }
        }
        return output.stream();
    }
}

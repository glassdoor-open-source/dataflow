/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FilteredStreamValueProvider.java
 */

package dataflow.core.component.stream;

import java.util.stream.Stream;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ValueType;

import static dataflow.core.parser.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;

@DataFlowComponent
public class FilteredStreamValueProvider {

    private final boolean parallel;
    private final String itemValueName;

    @DataFlowConfigurable
    public FilteredStreamValueProvider(
            @DataFlowConfigProperty(required = false) String itemValueName,
            @DataFlowConfigProperty(required = false) Boolean parallel) {
        this.itemValueName = itemValueName != null ? itemValueName : DEFAULT_ITEM_VALUE_NAME;
        this.parallel = parallel != null ? parallel : false;
    }

    @OutputValue
    public Stream<?> getValue(@InputValue Stream<?> stream, @InputValue DataFlowInstance filterDataFlow) {
        if (parallel) {
            // TODO(thorntonv): Support parallel execution outside of the common fork-join pool.
            stream = stream.parallel();
        }
        final DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        if (executionContext == null) {
            throw new RuntimeException("Execution context is not set");
        }
        return stream.filter(item -> executionContext.execute(() -> {
            filterDataFlow.setValue(itemValueName, item);
            filterDataFlow.execute();
            Object result = filterDataFlow.getOutput();
            result = executionContext.getEngine().convert(result, ValueType.of(result), ValueType.of(Boolean.class));
            return ((Boolean) result);
        }));
    }
}

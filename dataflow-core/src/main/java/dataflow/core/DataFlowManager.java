/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowManager.java
 */

package dataflow.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;

import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceEventListener;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.DataFlowParser;

public class DataFlowManager {

    private class EventListener implements DataSourceEventListener<DataSource> {

        @Override
        public void onAdd(DataSource dataSource) {
            try {
                engine.registerDataFlow(readConfig(dataSource));
            } catch (IOException | DataFlowParseException e) {
                onError(e);
            }
        }

        @Override
        public void onChange(DataSource dataSource) {
            try {
                engine.updateDataFlow(readConfig(dataSource));
            } catch (IOException | DataFlowParseException e) {
                onError(e);
            }
        }

        @Override
        public void onRemove(DataSource dataSource) {
            try {
                engine.unregisterDataFlow(readConfig(dataSource).getId());
            } catch (IOException | DataFlowParseException e) {
                onError(e);
            }
        }

        @Override
        public void onError(Exception error) {
            // TODO(thorntonv): Implement method.
            error.printStackTrace();
        }
    }

    private final DataFlowParser parser;
    private final DataFlowEngine engine;
    private DataSourceManager<DataSource> dataSourceManager = new DataSourceManager<>();
    private long refreshIntervalSeconds = 300;

    public DataFlowManager() {
        this(new DataFlowEngine());
    }

    public DataFlowManager(DataFlowEngine engine) {
        this(new DataFlowParser(engine), engine);
    }

    public DataFlowManager(DataFlowParser parser, DataFlowEngine engine) {
        this.parser = parser;
        this.engine = engine;
        dataSourceManager.addEventListener(new EventListener());
    }

    public void registerConfigProvider(Callable<List<DataSource>> configProvider) {
        dataSourceManager.register(configProvider, refreshIntervalSeconds);
    }

    public DataFlowEngine getEngine() {
        return engine;
    }

    private DataFlowConfig readConfig(DataSource dataSource) throws IOException, DataFlowParseException {
        try (InputStream in = dataSource.getInputStream()) {
            return parser.parse(in);
        }
    }
}

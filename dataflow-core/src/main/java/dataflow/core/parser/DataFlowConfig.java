/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfig.java
 */

package dataflow.core.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DataFlowConfig {

    public static final String DEFAULT_ITEM_VALUE_NAME = "item";

    private String id;
    private final Map<String, ComponentConfig> components;
    private ComponentConfig output;
    private boolean eventsEnabled;
    private Map<String, DataFlowConfig> childDataFlows;

    public DataFlowConfig(final String id,
            final Map<String, ComponentConfig> components, final ComponentConfig output,
            final Map<String, DataFlowConfig> childDataFlows, final boolean eventsEnabled) {
        this.id = id;
        this.components = new HashMap<>(components);
        this.output = output;
        this.childDataFlows = new HashMap<>(childDataFlows);
        this.eventsEnabled = eventsEnabled;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Map<String, ComponentConfig> getComponents() {
        return components;
    }

    public ComponentConfig getOutput() {
        return output;
    }

    public Map<String, DataFlowConfig> getChildDataFlows() {
        return childDataFlows;
    }

    public boolean eventsEnabled() {
        return eventsEnabled;
    }

    public Set<String> getProvidedValueIds() {
        return components.values().stream()
                .filter(v -> v.getType() == null)
                .map(ComponentConfig::getId)
                .collect(Collectors.toSet());
    }

    public Set<String> getSinks() {
        return components.values().stream()
                .filter(ComponentConfig::isSink)
                .filter(v -> v.getOutputComponents().isEmpty())
                .filter(v -> !v.isAbstract())
                .filter(v -> v != output)
                .map(ComponentConfig::getId)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "DataFlowConfig{" +
                "id='" + id + '\'' +
                ", components=" + components +
                ", output=" + output +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataFlowConfig that = (DataFlowConfig) o;
        return eventsEnabled == that.eventsEnabled && Objects.equals(getId(), that.getId())
            && Objects.equals(getComponents(), that.getComponents()) && Objects
            .equals(getOutput(), that.getOutput()) && Objects
            .equals(getChildDataFlows(), that.getChildDataFlows());
    }

    @Override
    public int hashCode() {
        return Objects
            .hash(getId(), getComponents(), getOutput(), eventsEnabled, getChildDataFlows());
    }
}

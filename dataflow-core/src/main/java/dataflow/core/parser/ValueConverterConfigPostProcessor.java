/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueConverterConfigPostProcessor.java
 */

package dataflow.core.parser;

import dataflow.annotation.metadata.DataFlowComponentInputMetadata;
import dataflow.annotation.metadata.DataFlowComponentMetadata;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dataflow.core.engine.ValueType;

import static dataflow.core.engine.codegen.CodeGenerationUtil.PRIMITIVE_TYPE_MAP;

/**
 * Adds {@link dataflow.annotation.ValueConverter} value providers to convert inputs to the required types.
 */
public class ValueConverterConfigPostProcessor {

    public void process(DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        // TODO(thorntonv): Consider handling fallback value conversion
        // TODO(thorntonv): Consider handling dynamic property conversion

        List<ComponentConfig> componentConfigList = new ArrayList<>(ctx.componentConfigs.values());

        for (ComponentConfig componentConfig : componentConfigList) {
            Map<String, String> inputTypes = new HashMap<>(componentConfig.getInputTypes());
            if (componentConfig.getType() != null) {
                DataFlowComponentMetadata metadata = ctx.engine.getComponentMetadata(
                    componentConfig.getType());
                if (metadata.hasDynamicInput()) {
                    // The input is dynamic so no need to convert.
                    continue;
                }
                // Use the raw input type if an input type was not explicitly set in the config
                for (DataFlowComponentInputMetadata inputMetadata : metadata.getInputMetadata()) {
                    inputTypes.putIfAbsent(inputMetadata.getName(), inputMetadata.getRawType());
                }
            }
            for (Map.Entry<String, ComponentConfig> inputEntry : componentConfig.getInput().entrySet()) {
                String fromType = inputEntry.getValue().getOutputType();
                if (inputEntry.getValue().getType() != null) {
                    DataFlowComponentMetadata inputMetadata = ctx.engine.getComponentMetadata(
                            inputEntry.getValue().getType());
                    fromType = inputMetadata.getOutputMetadata().getRawType();
                }
                if (PRIMITIVE_TYPE_MAP.containsKey(fromType)) {
                    // The wrapper types are used for primitive value fields in the DataFlow instance.
                    fromType = PRIMITIVE_TYPE_MAP.get(fromType);
                }

                String toType = inputTypes.getOrDefault(inputEntry.getKey(), Object.class.getName());
                if (toType != null && !toType.equals(Object.class.getName()) && !toType.equals(fromType)) {
                    String converterType = getValueConverterType(fromType, toType, ctx);
                    if (converterType == null) {
                        System.out.println(String.format(
                                "No converter available to convert from %s output type (%s) to %s input %s type (%s)",
                                inputEntry.getValue().getId(), fromType, componentConfig.getId(),
                                inputEntry.getKey(), toType));
                    } else {
                        ComponentConfig converterConfig = DataFlowConfigBuilder.getOrCreateComponentConfig(
                                "_" + inputEntry.getValue().getId() + "-as-" + toType.replace(".", "_"), ctx);
                        if (converterConfig.getType() == null) {
                            converterConfig.setType(converterType);
                            converterConfig.setInternal(true);
                            converterConfig.setInput("value", inputEntry.getValue());
                            ValueType fromTypeValue = ValueType.fromString(fromType);
                            ValueType toTypeValue = ValueType.fromString(toType);
                            Map<String, Object> properties = new HashMap<>();
                            properties.put("fromType", fromTypeValue);
                            properties.put("toType", toTypeValue);
                            converterConfig.setProperties(properties);
                        }
                        componentConfig.setInput(inputEntry.getKey(), converterConfig);
                    }
                }
            }
        }
    }

    private String getValueConverterType(String fromType, String toType, DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        if (fromType == null) {
            fromType = Object.class.getName();
        }
        if (toType == null) {
            toType = Object.class.getName();
        }
        DataFlowComponentMetadata metadata = ctx.engine.getValueRegistry().getConverterValueProviderMetadata(fromType, toType);
        return metadata != null ? metadata.getTypeName() : null;
    }
}
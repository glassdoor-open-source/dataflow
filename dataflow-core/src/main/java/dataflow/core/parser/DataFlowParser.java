/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowParser.java
 */

package dataflow.core.parser;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowParseException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;

public class DataFlowParser {

    private final DataFlowConfigBuilder configBuilder;

    public DataFlowParser(DataFlowEngine engine) {
        this.configBuilder = new DataFlowConfigBuilder(engine);
    }

    public static class ConfigurableObjectConstructor extends SafeConstructor {

        public ConfigurableObjectConstructor(DataFlowEngine engine) {
            // Register all known data flow component and configurable object type names as YAML
            // tags. RawComponentConfig/RawConfigurableObjectConfig objects will be created in the
            // map output by the YAML parser when these tags are present. This is done to resolve
            // some cases where it would be ambiguous if the configuration is specifying a map value
            // or a component / configurable object config.

            Set<String> propertyObjectBuilders = new HashSet<>(engine.getPropertyObjectBuilders().keySet());
            for(DataFlowComponentMetadata metadata : engine.getAllComponentMetadata()) {
                propertyObjectBuilders.remove(metadata.getTypeName());
                this.yamlConstructors.put(new Tag("!" + metadata.getTypeName()),
                    new ConstructComponent(metadata.getTypeName()));
            }
            for (String type : new String[]{"ProvidedValue", "Component", "ValueProvider"}) {
                this.yamlConstructors.put(new Tag("!" + type),
                    new ConstructComponent(type));
            }

            for(String dataFlowConfigurable : propertyObjectBuilders) {
                this.yamlConstructors.put(new Tag("!" + dataFlowConfigurable),
                    new ConstructDataFlowConfigurableObject(dataFlowConfigurable));
            }
        }

        private class ConstructComponent extends AbstractConstruct {

            private String type;

            ConstructComponent(String type) {
                this.type = type;
            }

            public Object construct(Node node) {
                Map mapping = constructMapping((MappingNode) node);
                return new RawComponentConfig(type, mapping);
            }
        }

        private class ConstructDataFlowConfigurableObject extends AbstractConstruct {

            private String type;

            ConstructDataFlowConfigurableObject(String type) {
                this.type = type;
            }

            public Object construct(Node node) {
                Map obj = new HashMap<>();
                if (node instanceof MappingNode) {
                    obj = constructMapping((MappingNode) node);
                }
                return new RawConfigurableObjectConfig(type, obj);
            }
        }
    }


    public DataFlowConfig parse(InputStream in) throws DataFlowConfigurationException, DataFlowParseException {
        if(in == null) {
            throw new IllegalArgumentException("Attempted to parse DataFlow config with a null input stream");
        }
        Map<String, Object> map;
        try {
            Yaml yaml = new Yaml(new ConfigurableObjectConstructor(configBuilder.getEngine()));
            map = yaml.load(in);
        } catch (Exception ex) {
            throw new DataFlowParseException("Failed to parse DataFlow configuration: " + ex.getMessage(), ex);
        }
        return configBuilder.build(map);
    }
}

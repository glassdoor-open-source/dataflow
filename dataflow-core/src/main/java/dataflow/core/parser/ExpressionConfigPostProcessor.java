/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExpressionConfigPostProcessor.java
 */

package dataflow.core.parser;

import java.io.StringReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.component.ExpressionValueProviderMetadata;

import static dataflow.core.parser.DataFlowConfigBuilder.getOrCreateComponentConfig;
import static dataflow.core.component.ExpressionValueProviderCodeGenerator.EXPRESSION_PROPERTY_NAME;

public class ExpressionConfigPostProcessor {

    private static class ExpressionParseContext {

        ComponentConfig componentConfig;
        StringBuilder expression = new StringBuilder();
        StringBuilder id = new StringBuilder();
        StringBuilder properties = new StringBuilder();
        StringBuilder inputs = new StringBuilder();
        StringBuilder current = expression;
        boolean inExpr = false;
        // true if this is a reference to an existing value, otherwise a new value provider will be created.
        boolean valueRef = true;

        public ExpressionParseContext(final ComponentConfig componentConfig) {
            this.componentConfig = componentConfig;
        }
    }

    private Yaml yaml = new Yaml();

    void process(DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        List<ComponentConfig> configs = new ArrayList<>(ctx.componentConfigs.values());
        for (ComponentConfig componentConfig : configs) {
            if (ExpressionValueProviderMetadata.TYPE.equalsIgnoreCase(componentConfig.getType())) {
                processExpressionValueProvider(componentConfig, ctx);
            }
        }
    }

    private void processExpressionValueProvider(ComponentConfig componentConfig,
            DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        String expr = (String) componentConfig.getProperties().get(EXPRESSION_PROPERTY_NAME);
        if(expr.contains("$(")) {
            throw new DataFlowConfigurationException("Invalid expression " + expr + " expressions must use @ syntax");
        }
        ExpressionParseContext context = new ExpressionParseContext(componentConfig);
        Deque<ExpressionParseContext> stack = new ArrayDeque<>();
        for (int idx = 0; idx < expr.length(); idx++) {
            try {
                char ch = expr.charAt(idx);
                if (ch == '@') {
                    if (context.inExpr) {
                        stack.addLast(context);
                        context = new ExpressionParseContext(getOrCreateComponentConfig(
                                ctx.idGenerator.generate(context.componentConfig.getId() + "_inner"), ctx));
                        context.componentConfig.setType(ExpressionValueProviderMetadata.TYPE);
                    }
                    context.current = context.id;
                    context.inExpr = true;
                    continue;
                } else if (context.inExpr && ch == '{') {
                    context.valueRef = false;
                    context.current = context.properties;
                    continue;
                } else if (context.inExpr && ch == '}') {
                    context.current = null;
                    continue;
                } else if (context.inExpr && ch == '(') {
                    if (context.id.length() > 0) {
                        context.valueRef = false;
                        context.current = context.inputs;
                    }
                    continue;
                } else if (context.inExpr && ch == ')') {
                    finalizeExpression(context, ctx);

                    if (!stack.isEmpty()) {
                        finalizeExpressionValueProviderConfig(context);
                        ExpressionParseContext innerContext = context;
                        context = stack.pop();
                        String refId = innerContext.componentConfig.getId();
                        if(isSimpleValueReferenceExpr(innerContext.componentConfig)) {
                            // The inner expression is simply a reference to a value. Skip evaluating the inner
                            // expression remove it and just reference the value directly.
                            ctx.componentConfigs.remove(innerContext.componentConfig.getId());
                            innerContext.componentConfig.getInput().values().forEach(input ->
                                    input.getOutputComponents().remove(innerContext.componentConfig
                                        .getId()));
                            refId = innerContext.componentConfig.getInput().keySet().iterator().next();

                        }
                        context.current.append("$(").append(refId).append(")");
                    }
                    continue;
                }
                if (context.current == null) {
                    throw new RuntimeException("Invalid expression");
                }
                context.current.append(ch);
            } catch (Exception ex) {
                throw new DataFlowConfigurationException(
                        "Error parsing expression id=" + componentConfig.getId() +
                                " \"" + expr.substring(0, idx) + "\" -> \"" + expr.substring(idx) + "\"", ex);
            }
        }

        finalizeExpressionValueProviderConfig(context);
    }

    private void finalizeExpression(ExpressionParseContext context, DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        String id = context.id.toString();
        if(id == null || id.trim().isEmpty()) {
            throw new DataFlowConfigurationException("Id not specified for expression");
        }
        ComponentConfig configRef = ctx.componentConfigs.get(id);
        Map<String, Object> inputs = toMap(context.inputs.toString());
        Map<String, Object> properties = toMap(context.properties.toString());
        if (configRef == null && context.valueRef) {
            // Create a config to reference the externally provided value.
            configRef = getOrCreateComponentConfig(id, ctx);
        } else if (configRef == null || !inputs.isEmpty() || !properties.isEmpty()) {
            // Create a new config.
            Map<String, Object> newConfig = new HashMap<>();
            newConfig.put("id", ctx.idGenerator.generate(context.componentConfig.getId() + "_inner"));
            String type;
            if (configRef != null) {
                newConfig.put("extends", configRef.getId());
                type = "ValueProvider";
            } else {
                newConfig.put("type", id);
                type = id;
            }
            if (inputs.size() == 1 && inputs.values().iterator().next() == null) {
                // A single input without an associated name. Use the name value as default.
                inputs.put("value", inputs.keySet().iterator().next());
            }
            newConfig.put("input", inputs);
            newConfig.putAll(properties);

            RawComponentConfig rawComponentConfig = new RawComponentConfig(type, newConfig);
            configRef = DataFlowConfigBuilder.buildComponent(rawComponentConfig, ctx);
        }

        if (configRef == null) {
            throw new DataFlowConfigurationException("Invalid id " + id);
        }

        context.inExpr = false;
        context.id.setLength(0);
        context.inputs.setLength(0);
        context.properties.setLength(0);
        context.expression.append("@(").append(configRef.getId()).append(")");
        context.componentConfig.setInput(configRef.getId(), configRef);
        context.current = context.expression;
    }

    private Map<String, Object> toMap(String str) {
        if (str == null || str.trim().isEmpty()) {
            return Collections.emptyMap();
        }
        return yaml.load(new StringReader("{" + str + "}"));
    }

    private void finalizeExpressionValueProviderConfig(ExpressionParseContext context) {
        context.componentConfig.getProperties().put(EXPRESSION_PROPERTY_NAME,
                context.expression.toString());
    }

    private boolean isSimpleValueReferenceExpr(ComponentConfig expressionConfig) {
        if (!ExpressionValueProviderMetadata.TYPE.equalsIgnoreCase(expressionConfig.getType())) {
            return false;
        }
        String expr = (String) expressionConfig.getProperties().get(EXPRESSION_PROPERTY_NAME);
        return expressionConfig.getInput().size() == 1 && expr.startsWith("@(") && expr.endsWith(")");
    }
}

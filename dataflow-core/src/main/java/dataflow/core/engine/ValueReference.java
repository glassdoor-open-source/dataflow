/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueReference.java
 */

package dataflow.core.engine;

import java.util.Objects;

/**
 * A reference to the output value of a value provider.
 */
public class ValueReference {

    private String componentId;

    public ValueReference(final String componentId) {
        this.componentId = componentId;
    }

    public String getComponentId() {
        return componentId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final ValueReference that = (ValueReference) o;
        return Objects.equals(componentId, that.componentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(componentId);
    }

    @Override
    public String toString() {
        return "ValueReference{" +
                "componentId='" + componentId + '\'' +
                '}';
    }
}

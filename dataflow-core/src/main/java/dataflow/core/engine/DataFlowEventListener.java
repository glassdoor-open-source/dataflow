/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEventListener.java
 */

package dataflow.core.engine;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DataFlowEventListener {

    public void onDataFlowLoad(final String dataFlowId, final long timestamp) {}

    public void onDataFlowLoadError(final String dataFlowId, final Throwable error) {}

    public void onDataFlowChanged(final String dataFlowId, final long timestamp) {}

    public void onDataFlowUnload(final String dataFlowId, final long timestamp) {}

    public void onDataFlowInstanceCreated(final String dataFlowId, final DataFlowInstance instance,
            final long timestamp) {}

    public void onDataFlowExecutionSuccess(String dataFlowId, String instanceId,
            long startTimeMillis, long endTimeMillis) {}

    public void onDataFlowExecutionError(String dataFlowId, String instanceId,
            long startTimeMillis, long endTimeMillis, Throwable error) {}

    public void onComponentExecutionSuccess(String dataFlowId, String instanceId, String componentId,
            long startTimeMillis, long endTimeMillis) {}

    public void onComponentExecutionError(String dataFlowId, String instanceId, String componentId,
            long startTimeMillis, long endTimeMillis, Throwable error) {}

    public void onComponentFallback(final String dataFlowId, final String instanceId,
            final String componentId, final String fallbackComponentId) {}

    public void onComponentErrorFallback(final String dataFlowId, final String instanceId,
            final String componentId, final String fallbackComponentId, final Throwable error) {}
}

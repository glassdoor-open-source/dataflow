/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowExecutionContext.java
 */

package dataflow.core.engine;

import dataflow.core.parser.DataFlowConfig;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Holds the execution context of a DataFlow.
 */
public class DataFlowExecutionContext {

    private static final ThreadLocal<DataFlowExecutionContext> currentExecutionContext = new ThreadLocal<>();

    private DataFlowConfig dataFlowConfig;
    private DataFlowInstance instance;
    private DataFlowEngine engine;

    /**
     * The execution context from which this context was forked or null if there was no prior context.
     */
    private DataFlowExecutionContext parentContext;

    public static DataFlowExecutionContext getCurrentExecutionContext() {
        return currentExecutionContext.get();
    }

    /**
     * Creates a new execution context and sets it to the current context.
     */
    public static DataFlowExecutionContext createExecutionContext(DataFlowConfig dataFlowConfig, DataFlowEngine engine) {
        DataFlowExecutionContext parentContext = getCurrentExecutionContext();
        DataFlowExecutionContext context = new DataFlowExecutionContext(engine);
        context.setParentContext(parentContext);
        context.setDataFlowConfig(dataFlowConfig);
        currentExecutionContext.set(context);
        return context;
    }

    public static void popExecutionContext() {
        DataFlowExecutionContext context = getCurrentExecutionContext();
        if (context != null) {
            currentExecutionContext.set(context.getParentContext());
        }
    }

    public static void setCurrentExecutionContext(DataFlowExecutionContext executionContext) {
        currentExecutionContext.set(executionContext);
    }

    DataFlowExecutionContext(DataFlowEngine engine) {
        this.engine = engine;
    }

    public String getDataFlowId() {
        return dataFlowConfig.getId();
    }

    public DataFlowEngine getEngine() {
        return engine;
    }

    public DataFlowExecutionContext getParentContext() {
        return parentContext;
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public void setDataFlowConfig(final DataFlowConfig dataFlowConfig) {
        this.dataFlowConfig = dataFlowConfig;
    }

    public DataFlowInstance getInstance() {
        return instance;
    }

    public void setInstance(final DataFlowInstance instance) {
        this.instance = instance;
    }

    public void setParentContext(final DataFlowExecutionContext parentContext) {
        this.parentContext = parentContext;
    }

    public DataFlowEngine.DependencyInjector getDependencyInjector() {
        return engine.getDependencyInjector();
    }

    public Object buildObject(final String type, final Map<String, Object> propValues,
            final Map<String, Object> instanceValues) {
        return engine.buildObject(type, propValues, instanceValues);
    }

    public List buildListObject(List list, String itemClassName, Map<String, Object> instanceValues) {
        return engine.buildListObject(list, itemClassName, instanceValues);
    }

    /**
     * Executes the given callable using this execution context. The current execution context is restored after the
     * call is complete.
     */
    public <T> T execute(Callable<T> function) {
        try {
            DataFlowExecutionContext currentContext = DataFlowExecutionContext.getCurrentExecutionContext();
            DataFlowExecutionContext.setCurrentExecutionContext(this);
            try {
                return function.call();
            } finally {
                DataFlowExecutionContext.setCurrentExecutionContext(currentContext);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void execute(Runnable function) {
        execute(() -> {
            function.run();
            return null;
        });
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PlaceholderString.java
 */

package dataflow.core.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A string with placeholders that can be replaced with other strings. A placeholder is defined using $(name)
 * <p>
 * For example the string:
 * abc$(value1)123
 * has a single placeholder value called value1.
 */
public class PlaceholderString {

    public static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("\\$\\((.*?)\\)");

    private final List<String> placeholderNames = new ArrayList<>();
    private final List<String> placeholderStringParts = new ArrayList<>();

    public PlaceholderString(String placeholderString) {
        // Break the string down into a list of placeholder names and a list of parts around the placeholders.
        // If the string starts with a placeholder then the first placeholder part should be an empty string.
        Matcher matcher = PLACEHOLDER_PATTERN.matcher(placeholderString);
        int previousMatchEnd = 0;
        while (matcher.find()) {
            placeholderNames.add(matcher.group(1));
            String part = placeholderString.substring(previousMatchEnd, matcher.start());
            placeholderStringParts.add(part);
            previousMatchEnd = matcher.end();
        }
        if (previousMatchEnd < placeholderString.length()) {
            placeholderStringParts.add(placeholderString.substring(previousMatchEnd));
        }
    }

    public List<String> getPlaceholderNames() {
        return placeholderNames;
    }

    /**
     * Converts the placeholder string to a string using the given replacement values for the placeholders.
     *
     * @param replacementValues the replacement values for the placeholders
     * @return the string with the placeholders replaced with the given values
     */
    public String toString(Map<String, Object> replacementValues) {
        if (!replacementValues.keySet().containsAll(placeholderNames)) {
            throw new IllegalArgumentException(String.format(
                    "%s Replacement values %s does not have entries for all placeholders",
                    toString(), replacementValues.toString()));
        }
        StringBuilder builder = new StringBuilder();
        Iterator<String> placeHolderNameIt = placeholderNames.iterator();
        for (String part : placeholderStringParts) {
            builder.append(part);
            if (placeHolderNameIt.hasNext()) {
                String name = placeHolderNameIt.next();
                Object value = replacementValues.get(name);
                value = value != null ? value.toString() : "";
                builder.append(value);
            }
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return "PlaceholderString{" +
                "placeholderNames=" + placeholderNames +
                ", placeholderStringParts=" + placeholderStringParts +
                '}';
    }
}

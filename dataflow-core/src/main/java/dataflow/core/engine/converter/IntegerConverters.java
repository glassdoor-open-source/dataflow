/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerConverters.java
 */

package dataflow.core.engine.converter;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.ValueConverter;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.value.IntegerValue;

public class IntegerConverters {

    private static final ValueConverterFunction<Integer, Integer> IDENTITY_CONVERTER = new ValueConverterFunction<Integer, Integer>() {
        @Override
        public Integer getValue(final Integer value) {
            return value;
        }
    };

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(Integer_to_int_converterMetadata.INSTANCE,
                new Integer_to_int_converterBuilder(engine));
        engine.getValueRegistry().register(Integer_to_int_converterMetadata.INSTANCE, IDENTITY_CONVERTER);

        engine.registerComponent(Integer_to_IntegerValue_converterMetadata.INSTANCE,
                new Integer_to_IntegerValue_converterBuilder(engine));
        engine.getValueRegistry().register(Integer_to_IntegerValue_converterMetadata.INSTANCE,
                new Integer_to_IntegerValue_converter());

        engine.registerComponent(Integer_to_String_converterMetadata.INSTANCE,
                new Integer_to_String_converterBuilder(engine));
        engine.getValueRegistry().register(Integer_to_String_converterMetadata.INSTANCE,
                new Integer_to_String_converter());

        engine.registerComponent(Short_to_Integer_converterMetadata.INSTANCE,
            new Short_to_Integer_converterBuilder(engine));
        engine.getValueRegistry().register(Short_to_Integer_converterMetadata.INSTANCE,
            new Short_to_Integer_converter());

    }

    @DataFlowComponent
    @ValueConverter
    public static final class Integer_to_int_converter {

        @DataFlowConfigurable
        public Integer_to_int_converter() {}

        @OutputValue
        public int getValue(@InputValue Integer input) {
            return input;
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class Integer_to_IntegerValue_converter implements ValueConverterFunction<Integer, IntegerValue> {

        @DataFlowConfigurable
        public Integer_to_IntegerValue_converter() {}

        @OutputValue
        public IntegerValue getValue(@InputValue Integer input) {
            return input != null ? new IntegerValue(input) : new IntegerValue();
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class Integer_to_String_converter implements ValueConverterFunction<Integer, String> {

        @DataFlowConfigurable
        public Integer_to_String_converter() {}

        @OutputValue
        public String getValue(@InputValue Integer input) {
            return String.valueOf(input);
        }
    }


    @DataFlowComponent
    @ValueConverter
    public static final class Short_to_Integer_converter implements ValueConverterFunction<Short, Integer> {

        @DataFlowConfigurable
        public Short_to_Integer_converter() {}

        @OutputValue
        public Integer getValue(@InputValue Short input) {
            return input.intValue();
        }
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Converters.java
 */

package dataflow.core.engine.converter;

import dataflow.core.engine.DataFlowEngine;

public class Converters {

    public static void register(DataFlowEngine engine) {
        DoubleConverters.register(engine);
        FloatValueConverters.register(engine);
        IntegerConverters.register(engine);
        IntegerValueConverters.register(engine);
        LongConverters.register(engine);
        StringConverters.register(engine);
        ListConverters.register(engine);
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DoubleConverters.java
 */

package dataflow.core.engine.converter;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import dataflow.annotation.ValueConverter;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.value.DoubleValue;

public class DoubleConverters {

    public static void register(DataFlowEngine engine) {
        engine.registerComponent(Double_to_double_converterMetadata.INSTANCE,
                new Double_to_double_converterBuilder(engine));
        engine.getValueRegistry().register(Double_to_double_converterMetadata.INSTANCE, IDENTITY_CONVERTER);

        engine.registerComponent(Double_to_DoubleValue_converterMetadata.INSTANCE,
                new Double_to_DoubleValue_converterBuilder(engine));
        engine.getValueRegistry().register(Double_to_DoubleValue_converterMetadata.INSTANCE, new Double_to_DoubleValue_converter());
    }

    private static final ValueConverterFunction<Double, Double> IDENTITY_CONVERTER = new ValueConverterFunction<Double, Double>() {
        @Override
        public Double getValue(final Double value) {
            return value;
        }
    };

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static class Double_to_double_converter {

        @DataFlowConfigurable
        public Double_to_double_converter() {}

        @OutputValue
        public double getValue(@InputValue final Double value) {
            return value;
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static class Double_to_DoubleValue_converter implements ValueConverterFunction<Double, DoubleValue> {

        @DataFlowConfigurable
        public Double_to_DoubleValue_converter() {}

        @Override
        @OutputValue
        public DoubleValue getValue(@InputValue final Double value) {
            return value != null ? new DoubleValue(value) : new DoubleValue();
        }
    }
}

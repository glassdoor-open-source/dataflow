/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEngine.java
 */

package dataflow.core.engine;

import static dataflow.core.engine.codegen.CodeGenerationUtil.isConstant;
import static dataflow.core.engine.codegen.CodeGenerationUtil.isProvidedValue;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.annotation.processor.TypeUtil;
import dataflow.core.compiler.CompileException;
import dataflow.core.compiler.JavaCompilerUtil;
import dataflow.core.component.CoreComponents;
import dataflow.core.data.DataComponents;
import dataflow.core.engine.codegen.ComponentCodeGenerator;
import dataflow.core.engine.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.engine.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.engine.converter.Converters;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.memorytable.MemoryTableDataFlowExtension;
import dataflow.core.memorytable.MemoryTableManager;
import dataflow.core.parser.ComponentConfig;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.RawConfigurableObjectConfig;
import dataflow.core.retry.RetryBuilders;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Executes a DataFlow.
 */
@SuppressWarnings({"unused", "unchecked"})
public class DataFlowEngine {

    /**
     * An interface for the dependency injection framework. Typically this will be implemented using Spring one of the
     * other commonly used dependency injection frameworks.
     */
    public interface DependencyInjector {

        /**
         * Get an instance of the specified class.
         */
        <T> T getInstance(Class<T> instanceClass);

        /**
         * Injects dependencies into the given object.
         */
        <T> T injectDependencies(T obj);
    }

    private interface DataFlowInstanceFactory {
        DataFlowInstance newInstance(Consumer<DataFlowInstance> instanceInitializationFunction)
            throws Exception;
    }

    private static class DataFlowRegistration {
        private String id;
        private final long createTimestamp = System.currentTimeMillis();
        private DataFlowConfig dataFlowConfig;
        private DataFlowInstanceFactory instanceFactory;
        private String source;
    }

    private static class ComponentRegistration {
        private DataFlowComponentMetadata metadata;
        private Class<?> componentClass;
        private DataFlowConfigurableObjectBuilder builder;
        private ComponentCodeGenerator codeGenerator;
    }

    private final Map<String, DataFlowRegistration> dataFlowRegistrationMap = new ConcurrentHashMap<>();
    private final Map<String, ComponentRegistration> componentRegistrationMap = new HashMap<>();
    private final Map<String, DataFlowConfigurableObjectBuilder> objectTypeBuilderMap = new HashMap<>();

    private final Executor executor;
    private final long defaultMaxExecutionTimeMillis = Long.MAX_VALUE;
    private final ValueRegistry valueRegistry;
    private final DataFlowInstanceCodeGenerator instanceCodeGenerator = new DataFlowInstanceCodeGenerator(this);
    private final DataFlowEventManager eventManager = new DataFlowEventManager();
    private final DependencyInjector dependencyInjector;

    private static final ThreadLocal<DataFlowExecutionContext> currentExecutionContext = new ThreadLocal<>();

    private final class DefaultDependencyInjector extends SimpleDependencyInjector {

        public DefaultDependencyInjector() {
            super(new MemoryTableManager(DataFlowEngine.this));
        }
    }

    public DataFlowEngine() {
        this(null);
    }

    public DataFlowEngine(DependencyInjector dependencyInjector) {
        this.executor = Executors.newFixedThreadPool(20);
        this.dependencyInjector = dependencyInjector != null ? dependencyInjector : new DefaultDependencyInjector();
        this.valueRegistry = new ValueRegistry();

        registerCoreComponents();
    }

    private void registerCoreComponents() {
        Converters.register(this);
        CoreComponents.register(this);
        DataComponents.register(this);
        MemoryTableDataFlowExtension.register(this);
        RetryBuilders.register(this);
    }

    public void registerComponent(DataFlowComponentMetadata metadata,
        DataFlowConfigurableObjectBuilder propertyObjectBuilder) {
        registerComponent(metadata, propertyObjectBuilder, null);
    }

    public void registerComponent(DataFlowComponentMetadata metadata,
        DataFlowConfigurableObjectBuilder propertyObjectBuilder,
            ComponentCodeGenerator codeGenerator) {
        String type = metadata.getTypeName();
        try {
            ComponentRegistration registration = new ComponentRegistration();
            registration.metadata = metadata;
            registration.componentClass = Class.forName(metadata.getComponentClassName());
            registration.builder = propertyObjectBuilder;
            registration.codeGenerator = codeGenerator;
            componentRegistrationMap.put(type, registration);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        objectTypeBuilderMap.put(type, propertyObjectBuilder);
    }

    public void registerPropertyObjectBuilder(Class<?> objClass, DataFlowConfigurableObjectBuilder builder) {
        objectTypeBuilderMap.put(objClass.getSimpleName(), builder);
    }

    public Map<String, DataFlowConfigurableObjectBuilder> getPropertyObjectBuilders() {
        return objectTypeBuilderMap;
    }

    public DataFlowInstance newDataFlowInstance(String dataFlowId) throws Exception {
        return newDataFlowInstance(dataFlowId, dataFlowInstance -> {});
    }

    public DataFlowInstance newDataFlowInstance(String dataFlowId,
        Consumer<DataFlowInstance> instanceInitializationFunction) throws Exception {
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();

        DataFlowRegistration registration = getDataFlowRegistration(dataFlowId);
        if (registration == null) {
            throw new IllegalArgumentException("DataFlow " + dataFlowId + " is not registered");
        }
        DataFlowInstance instance = registration.instanceFactory
            .newInstance(instanceInitializationFunction);
        instance.setMaxExecutionTimeMillis(defaultMaxExecutionTimeMillis);
        eventManager.fireInstanceCreationEvent(dataFlowId, instance, System.currentTimeMillis());
        return instance;
    }

    private DataFlowRegistration getDataFlowRegistration(String dataFlowId) {
        if (dataFlowId == null) {
            throw new IllegalArgumentException(
                "Attempted to get dataflow registration with a null id");
        }
        DataFlowRegistration registration = dataFlowRegistrationMap.get(dataFlowId);
        if(registration == null) {
            // Check for an id with timestamp match.
            registration = dataFlowRegistrationMap.values().stream()
                    .filter(reg -> reg.dataFlowConfig.getId().equals(dataFlowId))
                    .findAny().orElse(null);
            if(registration == null) {
                throw new IllegalArgumentException("DataFlow " + dataFlowId + " is not registered");
            }
        }
        return registration;
    }

    public synchronized void registerDataFlow(DataFlowConfig dataFlowConfig) throws DataFlowConfigurationException {
        if(dataFlowConfig == null || dataFlowConfig.getId() == null) {
            throw new DataFlowConfigurationException("DataFlow config must have an id: " + dataFlowConfig);
        }
        DataFlowRegistration registration = new DataFlowRegistration();
        DataFlowRegistration existingRegistration = dataFlowRegistrationMap.get(dataFlowConfig.getId());
        try {
            registration.id = dataFlowConfig.getId();
            registration.dataFlowConfig = dataFlowConfig;

            DataFlowInstanceCodeGenConfig codeGenConfig = DataFlowInstanceCodeGenConfig.builder()
                    .setPackage(getClass().getPackage().getName())
                    .setClassName(instanceCodeGenerator.getDefaultClassName(dataFlowConfig))
                    .build();

            Class<? extends DataFlowInstance> instanceClass = null;
            if (existingRegistration == null) {
                try {
                    // Use the already loaded instance class if one is available.
                    instanceClass = (Class<? extends DataFlowInstance>) Class.forName(codeGenConfig.getFullName());
                } catch (ClassNotFoundException e) {
                }
            }

            try {
                if (instanceClass == null) {
                    registration.source = instanceCodeGenerator.generateSource(dataFlowConfig, codeGenConfig);
                    instanceClass = JavaCompilerUtil.compile(codeGenConfig.getFullName(),
                        registration.source, this.getClass().getClassLoader());
                }
                final Constructor<? extends DataFlowInstance> constructor = instanceClass.getDeclaredConstructor(
                        DataFlowConfig.class, DataFlowEngine.class, Executor.class, Consumer.class);
                registration.instanceFactory = (initFn) ->
                        constructor.newInstance(dataFlowConfig, DataFlowEngine.this, executor, initFn);
            } catch (CompileException e) {
                throw new RuntimeException(e.getDiagnostics().toString() + "\n\nsource:\n\n" + registration.source, e);
            }
        } catch (Exception e) {
            eventManager.fireDataFlowLoadError(dataFlowConfig.getId(), e);
            throw new DataFlowConfigurationException("Unable to register dataflow " + dataFlowConfig.getId(), e);
        }

        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        boolean fireUnloadEvent = false;
        try {
            DataFlowExecutionContext newContext = DataFlowExecutionContext.createExecutionContext(dataFlowConfig, this);

            // Register child flows.
            dataFlowConfig.getChildDataFlows().values().forEach(this::registerDataFlow);

            // Make the dataflow available for use.
            dataFlowRegistrationMap.put(registration.id, registration);

            fireUnloadEvent = true;
            publishConfigLoadEvent(dataFlowConfig);

            if (existingRegistration != null) {
                unregisterDataFlow(existingRegistration);
            }
        } catch (Exception e) {
            for (String childId : dataFlowConfig.getChildDataFlows().keySet()) {
                try {
                    unregisterDataFlow(childId);
                } catch (Exception ex) {
                }
            }

            eventManager.fireDataFlowLoadError(dataFlowConfig.getId(), e);
            if(fireUnloadEvent) {
                publishConfigUnloadEvent(dataFlowConfig);
            }
            throw new RuntimeException(e);
        } finally {
            DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
        }
    }

    private void publishConfigLoadEvent(DataFlowConfig dataFlowConfig) throws Exception {
        for (ComponentConfig componentConfig : getSortedConfigs(dataFlowConfig)) {
            if (isProvidedValue(componentConfig) || isConstant(componentConfig)) {
                continue;
            }
            Class<?> componentClass = getComponentClass(componentConfig.getType());
            if (componentClass == null) {
                continue;
            }
            try {
                Method method = componentClass.getMethod("onLoad", ComponentConfig.class,
                        DataFlowConfig.class, DataFlowEngine.class);
                if (method != null) {
                    method.invoke(null, componentConfig, dataFlowConfig, this);
                }
            } catch (NoSuchMethodException ex) {
                // It is optional to define the onLoad method so this is not a problem.
            }
        }
        eventManager.fireDataFlowLoadEvent(dataFlowConfig.getId(), System.currentTimeMillis());
    }

    private void publishConfigUnloadEvent(final DataFlowConfig dataFlowConfig) {
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();

        try {
            DataFlowExecutionContext.createExecutionContext(dataFlowConfig, this);
            publishComponentEvents(dataFlowConfig, "onUnload");
            eventManager.fireDataFlowUnloadEvent(dataFlowConfig.getId(), System.currentTimeMillis());
        } catch (Exception e) {
            DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
            throw new RuntimeException("Failed to unload DataFlow " + dataFlowConfig.getId(), e);
        }
    }

    private void publishComponentEvents(DataFlowConfig dataFlowConfig, String eventMethodName) throws Exception {
        Exception error = null;
        for (ComponentConfig componentConfig : getSortedConfigs(dataFlowConfig)) {
            try {
                if (isProvidedValue(componentConfig) || isConstant(componentConfig)) {
                    continue;
                }
                Class<?> componentClass = getComponentClass(componentConfig.getType());
                if (componentClass == null) {
                    continue;
                }
                try {
                    Method method = componentClass.getMethod(eventMethodName, ComponentConfig.class,
                            DataFlowConfig.class, DataFlowEngine.class);
                    if (method != null) {
                        method.invoke(null, componentConfig, dataFlowConfig, this);
                    }
                } catch (NoSuchMethodException ex) {
                }
            } catch (Exception ex) {
                error = ex;
            }
        }
        if (error != null) {
            throw error;
        }
    }

    public List<ComponentConfig> getSortedConfigs(DataFlowConfig dataFlowConfig) {
        // TODO(thorntonv): This can be optimized by re-ordering to start executing async components
        //  as early as possible.
        ComponentConfig output = new ComponentConfig();
        output.setId("finalOutput-" + UUID.randomUUID().toString());

        List<ComponentConfig> outputDependencies = new ArrayList<>();
        outputDependencies.add(dataFlowConfig.getOutput());
        // Add the sinks as dependencies of the output so they are included in the topological sort.
        dataFlowConfig.getSinks().forEach(id -> outputDependencies.add(dataFlowConfig.getComponents().get(id)));

        List<ComponentConfig> sortedConfigs = GraphUtil.topologicalSort(output, v -> {
            if(v == output) {
                return outputDependencies;
            }
            Map<String, ComponentConfig> deps = v.getDependencies();
            return deps.values();
        });
        sortedConfigs.remove(output);
        List<String> abstractConfigIds = sortedConfigs.stream()
                .filter(ComponentConfig::isAbstract)
                .map(ComponentConfig::getId)
                .collect(Collectors.toList());
        if (!abstractConfigIds.isEmpty()) {
            throw new DataFlowConfigurationException(abstractConfigIds +
                    " components are abstract and should not be used as an input or output");
        }
        Map<String, ComponentConfig> unusedConfigs = new HashMap<>(dataFlowConfig.getComponents());
        sortedConfigs.forEach(v -> unusedConfigs.remove(v.getId()));

        List<String> unusedNotAbstractConfigIds = unusedConfigs.values().stream()
                .filter(v -> !v.isAbstract())
                .filter(v -> v.getType() != null)
                .map(ComponentConfig::getId)
                .collect(Collectors.toList());
        if (!unusedNotAbstractConfigIds.isEmpty()) {
            throw new DataFlowConfigurationException(unusedNotAbstractConfigIds +
                    " components are not used and are not abstract");
        }
        return sortedConfigs;
    }

    public synchronized void updateDataFlow(DataFlowConfig dataFlowConfig) {
        registerDataFlow(dataFlowConfig);
        eventManager.fireDataFlowChangedEvent(dataFlowConfig.getId(), System.currentTimeMillis());
    }

    public synchronized void unregisterDataFlow(String dataFlowId) {
        unregisterDataFlow(getDataFlowRegistration(dataFlowId));
    }

    private synchronized void unregisterDataFlow(DataFlowRegistration registration) {
        // Unregister the DataFlow's children.
        for (String childId : registration.dataFlowConfig.getChildDataFlows().keySet()) {
            try {
                unregisterDataFlow(childId);
            } catch (Exception ex) {
            }
        }
        dataFlowRegistrationMap.remove(registration.id, registration);
        if (registration.dataFlowConfig != null) {
            publishConfigUnloadEvent(registration.dataFlowConfig);
        }
    }

    Object buildObject(String type, Map<String, Object> map, Map<String, Object> instanceValues) {
        DataFlowConfigurableObjectBuilder builder = objectTypeBuilderMap.get(type);
        if (builder == null) {
            throw new IllegalArgumentException("No builder registered for type " + type);
        }
        return builder.build(map, instanceValues);
    }

    /**
     * Builds a list using a list from a parsed configuration file. New configured instances will be
     * created for any items in the list that are {@link dataflow.annotation.DataFlowConfigurable}
     * objects. An attempt will be made to convert items to the type of the output list if needed.
     *
     * @param list the list from the parsed configuration file
     * @param itemClassName the item class of the output list
     * @param instanceValues component values for the dataflow instance.
     * @return the built list
     */
    List buildListObject(List list, String itemClassName, Map<String, Object> instanceValues) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        List result = new ArrayList();
        for (Object item : list) {
            if (item instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) item;
                item = buildObject(obj.getType(), obj, instanceValues);
            }
            ValueType itemType = ValueType.fromString(itemClassName);
            result.add(convert(item, ValueType.fromString(item.getClass().getName()), itemType));
        }
        return result;
    }

    /**
     * Builds a map using a map from a parsed configuration file. New configured instances will be
     * created for any keys or values in the map that are
     * {@link dataflow.annotation.DataFlowConfigurable} objects. An attempt will be made to convert
     * keys and values to the types of the output map if needed.
     *
     * @param map the map from the parsed configuration file
     * @param keyClassName the key class of the output map
     * @param valueClassName the value class of the output map
     * @param instanceValues component values for the dataflow instance.
     * @return the built map
     */
    Map buildMapObject(Map<?, ?> map, String keyClassName, String valueClassName, Map<String, Object> instanceValues) {
        if (map == null || map.isEmpty()) {
            return Collections.emptyMap();
        }
        Map result = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            Object key = entry.getKey();
            if (key instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) key;
                key = buildObject(obj.getType(), obj, instanceValues);
            }
            ValueType keyType = ValueType.fromString(keyClassName);
            key = convert(key, ValueType.fromString(key.getClass().getName()), keyType);

            Object value = entry.getValue();
            if (value instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) value;
                value = buildObject(obj.getType(), obj, instanceValues);
            }
            ValueType valueType = ValueType.fromString(valueClassName);
            value = convert(value, ValueType.fromString(value.getClass().getName()), valueType);

            result.put(key, value);
        }
        return result;
    }

    DependencyInjector getDependencyInjector() {
        return dependencyInjector;
    }

    public String getSource(String dataFlowId) {
        DataFlowRegistration registration = dataFlowRegistrationMap.get(dataFlowId);
        return registration != null ? registration.source : null;
    }

    public <T, R> R convert(T object, ValueType fromValueType, ValueType toValueType) {
        return valueRegistry.convert(object, fromValueType, toValueType);
    }

    public <T, R> R convert(T object, Class<T> fromType, Class<R> toType) {
        return valueRegistry.convert(object, ValueType.fromString(fromType.getName()),
                ValueType.fromString(toType.getName()));
    }

    public <T, R> R convert(T object, Class<R> toType) {
        return valueRegistry.convert(object, ValueType.fromString(object.getClass().getName()),
                ValueType.fromString(toType.getName()));
    }

    public <T, R> R convert(T object, ValueType toValueType) {
        if (object == null) {
            return null;
        }
        return valueRegistry.convert(object, ValueType.fromString(object.getClass().getName()), toValueType);
    }

    public <T, R> boolean canConvert(Class<T> fromType, Class<R> toType) {
        return valueRegistry.canConvert(fromType.getName(), toType.getName());
    }

    public Class<?> getValueTypeClass(final String valueType) {
        return valueRegistry.getValueTypeClass(valueType);
    }

    public Class<?>[] getGenericTypeClasses(final String valueType) {
        String[] genericTypes = TypeUtil.getGenericTypes(valueType);
        Class<?>[] genericTypeClasses = new Class<?>[genericTypes.length];
        for (int idx = 0; idx < genericTypes.length; idx++) {
            genericTypeClasses[idx] = getValueTypeClass(genericTypes[idx]);
        }
        return genericTypeClasses;
    }

    public ValueRegistry getValueRegistry() {
        return valueRegistry;
    }

    public Class<?> getComponentClass(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        if (registration == null) {
            throw new IllegalArgumentException("No class available for type " + type);
        }
        return registration.componentClass;
    }

    public DataFlowComponentMetadata getComponentMetadata(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        if (registration == null) {
            throw new IllegalArgumentException("No metadata available for type " + type);
        }
        return registration.metadata;
    }

    public List<DataFlowComponentMetadata> getAllComponentMetadata() {
        List<DataFlowComponentMetadata> metadataList = new ArrayList<>();
        componentRegistrationMap.forEach((type, registration) ->
            metadataList.add(registration.metadata));
        return metadataList;
    }

    public ComponentCodeGenerator getComponentCodeGenerator(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        return registration != null ? registration.codeGenerator : null;
    }

    public DataFlowEventManager getEventManager() {
        return eventManager;
    }
}

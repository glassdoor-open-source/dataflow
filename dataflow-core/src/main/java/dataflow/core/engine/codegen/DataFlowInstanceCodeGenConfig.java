/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceCodeGenConfig.java
 */

package dataflow.core.engine.codegen;

import java.util.Objects;

public final class DataFlowInstanceCodeGenConfig {

    public static class Builder {

        private String packageName;
        private String className;
        private boolean incrementalEnabled = true;
        private boolean eventsEnabled = true;
        private boolean timeoutEnabled = true;

        public Builder setPackage(String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder setClassName(String className) {
            this.className = className;
            return this;
        }

        public Builder setIncrementalEnabled(boolean incrementalEnabled) {
            this.incrementalEnabled = incrementalEnabled;
            return this;
        }

        public Builder setEventsEnabled(boolean eventsEnabled) {
            this.eventsEnabled = eventsEnabled;
            return this;
        }

        public Builder setTimeoutEnabled(boolean timeoutEnabled) {
            this.timeoutEnabled = timeoutEnabled;
            return this;
        }

        public DataFlowInstanceCodeGenConfig build() {
            return new DataFlowInstanceCodeGenConfig(packageName, className, incrementalEnabled, eventsEnabled,
                    timeoutEnabled);
        }
    }

    public static Builder builder() {
        return new DataFlowInstanceCodeGenConfig.Builder();
    }

    private String packageName;
    private String className;
    private boolean incrementalEnabled;
    private boolean eventsEnabled;
    private boolean timeoutEnabled;

    public DataFlowInstanceCodeGenConfig(final String packageName, final String className,
            final boolean incrementalEnabled,
            final boolean eventsEnabled, final boolean timeoutEnabled) {
        this.packageName = packageName;
        this.className = className;
        this.incrementalEnabled = incrementalEnabled;
        this.eventsEnabled = eventsEnabled;
        this.timeoutEnabled = timeoutEnabled;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() {
        return className;
    }

    public String getFullName() {
        return getPackageName() + "." + getClassName();
    }

    public boolean isIncrementalEnabled() {
        return incrementalEnabled;
    }

    public boolean isEventsEnabled() {
        return eventsEnabled;
    }

    public boolean isTimeoutEnabled() {
        return timeoutEnabled;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final DataFlowInstanceCodeGenConfig that = (DataFlowInstanceCodeGenConfig) o;
        return incrementalEnabled == that.incrementalEnabled &&
                eventsEnabled == that.eventsEnabled &&
                timeoutEnabled == that.timeoutEnabled &&
                Objects.equals(packageName, that.packageName) &&
                Objects.equals(className, that.className);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageName, className, incrementalEnabled, eventsEnabled, timeoutEnabled);
    }

    @Override
    public String toString() {
        return "DataFlowInstanceCodeGenConfig{" +
                "packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", incrementalEnabled=" + incrementalEnabled +
                ", eventsEnabled=" + eventsEnabled +
                ", timeoutEnabled=" + timeoutEnabled +
                '}';
    }
}


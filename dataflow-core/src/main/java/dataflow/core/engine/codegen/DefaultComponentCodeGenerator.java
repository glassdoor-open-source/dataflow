/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DefaultComponentCodeGenerator.java
 */

package dataflow.core.engine.codegen;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.core.engine.PlaceholderString;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import dataflow.annotation.metadata.DataFlowComponentInputMetadata;
import dataflow.annotation.processor.IndentPrintWriter;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.parser.ComponentConfig;
import dataflow.core.value.BooleanValue;
import dataflow.core.value.DoubleValue;
import dataflow.core.value.FloatValue;
import dataflow.core.value.IntegerValue;
import dataflow.core.value.LongValue;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static dataflow.core.engine.codegen.CodeGenerationUtil.*;

/**
 * The default {@link ComponentCodeGenerator} that is used if the component does not have a custom
 * code generator.
 */
@SuppressWarnings("WeakerAccess")
public class DefaultComponentCodeGenerator implements ComponentCodeGenerator {

    private static final String[] IMPORTS = new String[]{
            BooleanValue.class.getName(),
            IntegerValue.class.getName(),
            LongValue.class.getName(),
            FloatValue.class.getName(),
            DoubleValue.class.getName(),
            AutoCloseable.class.getName()
    };

    private final MapValueProviderCodeGenerator inputMapCodeGenerator;

    public DefaultComponentCodeGenerator() {
        this(new MapValueProviderCodeGenerator());
    }

    public DefaultComponentCodeGenerator(MapValueProviderCodeGenerator inputMapCodeGenerator) {
        this.inputMapCodeGenerator = inputMapCodeGenerator;
    }

    @Override
    public Set<String> getImports() {
        Set<String> retVal = new HashSet<>(Arrays.asList(IMPORTS));
        if (inputMapCodeGenerator != null) {
            retVal.addAll(inputMapCodeGenerator.getImports());
        }
        return retVal;
    }

    @Override
    public void writeFields(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        String id = context.getId();
        DataFlowComponentMetadata componentMetadata = context.getComponentMetadata();
        // Write the component field.
        out.printf("private %s %s_component;%n", componentMetadata.getComponentClassName().replace("$", "."), getVarName(id));

        // Write the component value field
        writeValueField(out, id, context.getDataFlowContext());

        if (inputMapCodeGenerator != null && context.getComponentMetadata().hasDynamicInput()) {
            // Write fields used for the input map when the component has dynamic input.
            inputMapCodeGenerator.writeFields(out, context);
        }
    }

    /**
     * Writes the component value field.
     */
    public static void writeValueField(final IndentPrintWriter out, String id, DataFlowCodeGenerationContext context) {
        if (!context.hasPrimitiveOutputType(id)) {
            out.printf("private volatile %s %s_value;%n", context.getFieldType(id), getVarName(id));
        } else {
            out.printf("private volatile %s %s_value = new %s();%n",
                    context.getFieldType(id), getVarName(id), context.getFieldType(id));
        }
    }

    @Override
    public void writeConstructorStatements(final IndentPrintWriter out,
            final ComponentCodeGenerationContext context) {
        String id = context.getId();
        if (isConstant(context.getComponentConfig())) {
            out.printf(
                    "this.%s_value = (%s) buildConstant(dataFlowConfig.getComponents().get(\"%s\").getProperties().get(\"value\"));%n",
                    getVarName(id), context.getComponentMetadata().getOutputMetadata().getRawType(), id);
            out.printf("this.%s_finished = true;%n", getVarName(id));
            return;
        }
        out.printf("this.%s_component = (%s) buildObject(\"%s\", " +
                        "dataFlowConfig.getComponents().get(\"%s\").getProperties(), this.getValues());%n",
                getVarName(id), context.getComponentMetadata().getComponentClassName().replace("$", "."),
                context.getComponentConfig().getType(), id);
        if (inputMapCodeGenerator != null && context.getComponentMetadata().hasDynamicInput()) {
            inputMapCodeGenerator.writeConstructorStatements(out, context);
        }
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        String varNamePrefix = context.getVarNamePrefix();

        if (inputMapCodeGenerator != null && context.getComponentMetadata().hasDynamicInput()) {
            // If the component has dynamic input then use the input map code generator to build the input map that
            // will be passed to the component getValue method.
            inputMapCodeGenerator.writeExecuteStatements(out, context);
        }

        // Generate the call to the component getValue method.
        String returnTypeCast = "";
        if (context.getComponentMetadata() != null &&
            context.getComponentMetadata().getOutputMetadata() != null &&
            context.getComponentMetadata().getOutputMetadata().getRawType() != null) {
            returnTypeCast = context.getComponentMetadata().getOutputMetadata().getRawType();
            if(context.getComponentMetadata().hasAsyncGet()) {
                returnTypeCast = "CompletableFuture<" + returnTypeCast + ">";
            }
            returnTypeCast = "(" + returnTypeCast + ")";
        }

        String getValueExpr = String.format("(%s%s_component%s)", returnTypeCast, varNamePrefix,
            providerGetMethodCall(context));
        String getValueExprType = context.getRawOutputType();
        if (context.getComponentMetadata().hasAsyncGet()) {
            // If the component has an asyncGet method (that returns a CompletableFuture) then set the value after
            // the async method has completed.
            out.printf("return %s.thenApply(r -> {%n", getValueExpr);
            out.indent();
            out.print(setValueStmt(context, "r", getValueExprType));
            out.printf("return %s_value;%n", varNamePrefix);
            out.unindent();
            out.println("});");
        } else {
            out.print(setValueStmt(context, getValueExpr, getValueExprType));
        }
    }

    @Override
    public void writeCloseStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        out.printf(
                "if(%s_component != null && (%s_component instanceof AutoCloseable)) ((AutoCloseable) %s_component).close();%n",
                context.getVarNamePrefix(), context.getVarNamePrefix(), context.getVarNamePrefix());
    }

    private String providerGetMethodCall(ComponentCodeGenerationContext context) {
        return String.format(".getValue(%s)", buildProviderGetMethodParams(context));
    }

    /**
     * Builds the parameters string for a call to a component getValue method.
     *
     * @param context the context for the component for which we will build the getValue method parameters
     */
    private String buildProviderGetMethodParams(ComponentCodeGenerationContext context) {
        ComponentConfig componentConfig = context.getComponentConfig();
        DataFlowComponentMetadata componentMetadata = context.getComponentMetadata();
        StringBuilder params = new StringBuilder();

        // For each input parameter.
        for (DataFlowComponentInputMetadata inputMetadata : componentMetadata.getInputMetadata()) {
            if (params.length() > 0) {
                params.append(", ");
            }
            ComponentConfig inputComponentConfig =
                    componentConfig.getInput().get(inputMetadata.getName());
            if (inputMapCodeGenerator != null && componentMetadata.hasDynamicInput()) {
                // Pass a map which contains all of the inputs to the getValue method
                params.append(String.format("%s_inputMap", context.getVarNamePrefix()));
            } else if (inputComponentConfig != null) {
                ComponentCodeGenerationContext inputContext =
                        context.getDataFlowContext().getComponentContext(inputComponentConfig);

                // Get an expression for the input value.
                String inputRefExpr = getValueReferenceExpression(inputComponentConfig, context);
                String exprType = inputContext.getFieldType();

                // Convert the input value to the desired type (if needed).
                inputRefExpr = CodeGenerationUtil.valueConvertExpr(exprType, inputMetadata.getRawType(),
                        inputRefExpr, context.getDataFlowContext());

                params.append(String.format("%s", inputRefExpr));
            } else if (!inputMetadata.isRequired()) {
                params.append("null");
            } else {
                throw new DataFlowConfigurationException(String.format("Input %s not defined for component %s",
                        inputMetadata.getName(), componentConfig.getId()));
            }
        }
        return params.toString();
    }

    protected String replacePlaceholders(String str, final ComponentCodeGenerationContext context) {
        if(str == null) {
            return null;
        }
        str = str.replaceAll(Pattern.quote("@"), "\\$");
        PlaceholderString placeHolderString = new PlaceholderString(str);
        Set<String> placeholderNames = new HashSet<>(placeHolderString.getPlaceholderNames());
        placeholderNames.forEach(i -> {
            if(!context.getComponentConfig().getInput().containsKey(i)) {
                throw new RuntimeException("Input " + i + " is not defined");
            }
        });
        str = placeHolderString.toString(placeholderNames.stream()
            .collect(Collectors.toMap(v -> v, v -> CodeGenerationUtil.getVarName(v) + "_value")));
        return str;
    }
}

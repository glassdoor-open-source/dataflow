/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListValueProviderCodeGenerator.java
 */

package dataflow.core.engine.codegen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataflow.annotation.processor.IndentPrintWriter;
import dataflow.core.engine.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.parser.ComponentConfig;

import static dataflow.core.engine.codegen.CodeGenerationUtil.getValueReferenceExpression;

public class ListValueProviderCodeGenerator implements ComponentCodeGenerator {

    public static final String TYPE = "ListValueProvider";

    private static final String[] IMPORTS = new String[]{
            List.class.getName(),
            ArrayList.class.getName()
    };

    @Override
    public Set<String> getImports() {
        return new HashSet<>(Arrays.asList(IMPORTS));
    }

    @Override
    public void writeFields(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        out.printf("private List %s_value = new ArrayList<>(%d);%n", context.getVarNamePrefix(),
                context.getComponentConfig().getInput().size());
    }

    @Override
    public void writeConstructorStatements(final IndentPrintWriter out,
            final ComponentCodeGenerationContext context) {
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        out.printf("%s_value.clear();%n", context.getVarNamePrefix());
        for (ComponentConfig inputConfig : context.getComponentConfig().getInput().values()) {
            out.printf("%s_value.add(%s);%n", context.getVarNamePrefix(),
                    getValueReferenceExpression(inputConfig, context));
        }
    }

    @Override
    public void writeCloseStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
    }
}

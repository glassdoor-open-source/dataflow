/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StringUtil.java
 */

package dataflow.core.util;

import java.util.List;
import java.util.StringJoiner;

/**
 * String utility methods.
 */
public class StringUtil {

    /**
     * Forms a new string by joining the given values using joinStr in between each.
     */
    public static String join(List<?> values, String joinStr) {
        StringBuilder builder = new StringBuilder();
        join(builder, values, joinStr);
        return builder.toString();
    }

    /**
     * Writes a string to the builder that is formed by joining the given values using joinStr in between each.
     */
    public static void join(StringBuilder out, List<?> values, String joinStr) {
        StringJoiner joiner = new StringJoiner(joinStr);
        values.forEach(v -> joiner.add(v.toString()));
        out.append(joiner.toString());
    }

    public static String truncate(String str, int maxLength) {
        return str.length() <= maxLength ? str : str.substring(0, maxLength - 3) + "...";
    }
}

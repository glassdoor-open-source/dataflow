/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DotFileGenerator.java
 */

package dataflow.core.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import dataflow.annotation.processor.IndentPrintWriter;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.parser.ComponentConfig;

public class DotFileGenerator {

    private static final String CONST_PREFIX = "_const";
    private static final String LIST_PREFIX = "_list";
    private static final String FALLBACK_LABEL = "fallback";

    public static String generateDOTFile(DataFlowConfig dataFlowConfig) {
        StringWriter stringOut = new StringWriter();
        IndentPrintWriter out = new IndentPrintWriter(new PrintWriter(stringOut));
        out.printf("digraph %s {%n", dataFlowConfig.getId());
        out.indent();
        Map<String, String> idLabelMap = new HashMap<>();
        generateDOTFile(dataFlowConfig.getOutput(), idLabelMap, out);
        generateNodeLabels(dataFlowConfig, idLabelMap, out);
        out.unindent();
        out.println("}");
        return stringOut.toString();
    }

    private static void generateNodeLabels(DataFlowConfig dataFlowConfig, Map<String, String> idLabelMap,
            IndentPrintWriter out) {
        for (Map.Entry<String, String> entry : idLabelMap.entrySet()) {
            String style = "";
            if (entry.getKey().equals(dataFlowConfig.getOutput().getId())) {
                style = "shape=diamond, ";
            }
            out.printf("%s [%slabel=< %s >];%n", toId(entry.getKey()), style, entry.getValue());
        }
    }

    private static void generateDOTFile(ComponentConfig componentConfig, Map<String, String> idLabelMap,
            IndentPrintWriter out) {
        for (Map.Entry<String, ComponentConfig> entry : componentConfig.getInput().entrySet()) {
            generateDOTFile(componentConfig, entry.getValue(), entry.getKey(), idLabelMap, out);
        }
        ComponentConfig previousConfig = componentConfig;
        componentConfig.getInput().values().forEach(v -> generateDOTFile(v, idLabelMap, out));
    }

    private static void generateDOTFile(ComponentConfig componentConfig, ComponentConfig inputConfig,
            String edgeLabel, Map<String, String> idLabelMap,
            IndentPrintWriter out) {
        String inputId = inputConfig.getId();
        if (edgeLabel.equals(inputId) || componentConfig.getId().startsWith(LIST_PREFIX)) {
            edgeLabel = "";
        }
        String style = "";
        if (FALLBACK_LABEL.equals(edgeLabel)) {
            style = "style=dashed, color=grey,";
        }
        idLabelMap.put(componentConfig.getId(), getLabel(componentConfig));
        idLabelMap.put(inputId, getLabel(inputConfig));
        out.printf("%s -> %s[%s label=\"%s\" ];%n", toId(inputId), toId(componentConfig.getId()), style, edgeLabel);
    }

    private static String getLabel(final ComponentConfig componentConfig) {
        String type = componentConfig.getType();
        String id = componentConfig.getId();
        if (id.toLowerCase().startsWith(CONST_PREFIX)) {
            id = componentConfig.getProperties().get("value").toString();
        } else if (id.toLowerCase().startsWith(LIST_PREFIX)) {
            id = null;
        }
        if (id != null && type != null) {
            return String.format("<b>%s</b><br/><i>%s</i>", id, type);
        } else if (id != null) {
            return String.format("<b>%s</b>", id);
        } else {
            return String.format("<i>%s</i>", type);
        }
    }

    private static String toId(String id) {
        return id.replace("-", "_");
    }
}

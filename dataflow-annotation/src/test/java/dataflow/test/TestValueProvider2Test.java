/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider2Test.java
 */

package dataflow.test;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import dataflow.annotation.metadata.DataFlowComponentMetadata;
import dataflow.core.engine.DataFlowEngine;

import static org.junit.Assert.*;

public class TestValueProvider2Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider2Metadata.INSTANCE;

    private TestValueProvider2Builder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider2Builder(engine);
    }

    @Test
    public void testMetadata() {
        assertTrue(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals("inputs", METADATA.getInputMetadata().get(0).getName());
        assertEquals("java.util.Map<java.lang.String,java.lang.Object>", METADATA.getInputMetadata().get(0).getRawType());

        assertEquals(String.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_dynamicInput() {
        TestValueProvider2 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("separator", ";")
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        assertEquals("in1=abc;in2=123", valueProvider.getValue(ImmutableMap.<String, Object>builder()
                .put("in1", "abc")
                .put("in2", "123")
                .build()));
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider9Test.java
 */

package dataflow.test;

import dataflow.annotation.metadata.DataFlowComponentMetadata;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.ValueReference;

import static dataflow.test.TestEnum.VALUE1;
import static dataflow.test.TestEnum.VALUE3;
import static org.junit.Assert.*;

public class TestValueProvider9Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider9Metadata.INSTANCE;

    private TestValueProvider9Builder builder;

    @Before
    public void setUp() {
        final DataFlowEngine engine = new DataFlowEngine();
        builder = new TestValueProvider9Builder(engine);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals("java.util.List<dataflow.test.TestEnum>", METADATA.getPropertyMetadataMap().get("testList").getRawType());
        assertFalse(METADATA.getPropertyMetadataMap().get("testList").isEnum());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals("java.util.List<dataflow.test.TestEnum>", METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_enumList() {
        TestValueProvider9 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testList", ImmutableList.of("VALUE1", "VALUE3"))
                .build(), Collections.emptyMap());
        List<TestEnum> testList = valueProvider.getValue();
        assertEquals(2, testList.size());
        assertEquals(VALUE1, testList.get(0));
        assertEquals(VALUE3, testList.get(1));
    }

    @Test
    public void testBuilder_enumList_missing() {
        try {
            builder.build(ImmutableMap.<String, Object>builder()
                    .build(), ImmutableMap.<String, Object>builder()
                    .build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

    @Test
    public void testBuilder_enumList_valueReference() {
        List expectedValue = ImmutableList.of(VALUE1, VALUE3);
        TestValueProvider9 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("testList", new ValueReference("myVal"))
                .build(), ImmutableMap.<String, Object>builder()
                .put("myVal", expectedValue)
                .build());

        assertEquals(expectedValue, valueProvider.getValue());
    }
}

package dataflow.test;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.OutputValue;

public class InvalidValueProvider_nested_not_static {

    @DataFlowComponent
    public class InvalidValueProvider_nested_not_static_nested {

        private String param;

        @DataFlowConfigurable
        public InvalidValueProvider_nested_not_static_nested(@DataFlowConfigProperty String param) {
            this.param = param;
        }

        @OutputValue
        public String getValue() {
            return param;
        }
    }
}

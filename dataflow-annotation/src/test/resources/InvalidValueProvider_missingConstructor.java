package dataflow.test;

import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingConstructor {

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}

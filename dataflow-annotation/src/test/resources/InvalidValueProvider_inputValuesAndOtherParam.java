package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.InputValues;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_inputValuesAndOtherParam {

    @DataFlowConfigurable
    public InvalidValueProvider_inputValuesAndOtherParam() {
    }

    @OutputValue
    public String getValue(@InputValues Map<String, Object> values, @InputValue String in1) {
        return null;
    }
}

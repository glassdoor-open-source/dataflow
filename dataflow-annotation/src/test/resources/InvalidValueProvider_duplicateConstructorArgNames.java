package dataflow.test;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_duplicateConstructorArgNames {

    @DataFlowConfigurable
    public InvalidValueProvider_duplicateConstructorArgNames(
            @DataFlowConfigProperty String param1,
            @DataFlowConfigProperty(name = "param1") String param2) {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}

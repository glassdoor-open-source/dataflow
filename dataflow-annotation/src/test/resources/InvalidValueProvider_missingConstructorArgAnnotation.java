package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingConstructorArgAnnotation {

    @DataFlowConfigurable
    public InvalidValueProvider_missingConstructorArgAnnotation(String param1) {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}

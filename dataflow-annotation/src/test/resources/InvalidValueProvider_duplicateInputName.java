package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_duplicateInputName {

    @DataFlowConfigurable
    public InvalidValueProvider_duplicateInputName() {
    }

    @OutputValue
    public String getValue(@InputValue String in1, @InputValue(name = "in1") String in2) {
        return null;
    }
}

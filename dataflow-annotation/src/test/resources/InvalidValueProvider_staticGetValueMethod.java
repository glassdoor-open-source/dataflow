package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_staticGetValueMethod {

    @DataFlowConfigurable
    public InvalidValueProvider_staticGetValueMethod() {
    }

    @OutputValue
    public static String getValue(@InputValue String in1) {
        return null;
    }
}

package dataflow.test;

import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;

@DataFlowComponent
public class InvalidValueProvider_missingOutputValueAnnotation {

    @DataFlowConfigurable
    public InvalidValueProvider_missingOutputValueAnnotation() {
    }

    public String getValue(@InputValue String in1) {
        return null;
    }
}

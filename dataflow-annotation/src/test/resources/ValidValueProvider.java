package dataflow.test;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.DataFlowComponent;
import dataflow.annotation.InputValue;
import dataflow.annotation.OutputValue;
import java.util.List;

@DataFlowComponent
public class ValidValueProvider {

    @DataFlowConfigurable
    public ValidValueProvider(@DataFlowConfigProperty String prop1,
            @DataFlowConfigProperty List<String> prop2) {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}

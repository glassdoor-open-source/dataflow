/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponent.java
 */

package dataflow.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Component annotation. This annotation should be used on each component class. Component classes should
 * have a single constructor annotated with {@link DataFlowConfigurable}.
 *
 * ValueProviders should have a single getValue method that is annotated with {@link OutputValue}
 * and whose parameters are annotated with {@link InputValue} or {@link InputValues}.
 *
 * Actions should have a single execute method that can be annotated with {@link OutputValue} if
 * the action returns a value or the return type can be void. The execute method parameters should
 * be annotated with {@link InputValue} or {@link InputValues}.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface DataFlowComponent {

    /**
     * Used to specify a custom name for the component. If not specified the class name will be used.
     */
    String typeName() default "";

    /**
     * Indicates if this component should be executed asynchronously. This is only a default
     * value and it is possible that this value could be overridden depending on the configuration.
     */
    boolean executeAsync() default false;

    String description() default "";

    /**
     * The class to use for dynamically generating execution code for this component.
     * If {@link Void} then no dynamic code generation will be performed.
     */
    Class codeGenerator() default Void.class;
}
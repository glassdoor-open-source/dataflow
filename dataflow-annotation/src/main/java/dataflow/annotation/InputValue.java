/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  InputValue.java
 */

package dataflow.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for component input values. This annotation should be added to each parameter of the
 * component's getValue/execute method.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.PARAMETER)
public @interface InputValue {

    /**
     * The name of the input value. If not specified then the name of the parameter in the
     * component's getValue/execute method is used.
     */
    String name() default "";

    /**
     * A list of supported types for this input. This is used for validation when loading a DataFlow. If no types are
     * specified then this constraint is not used for validation.
     */
    String[] supportedTypes() default {};

    String description() default "";

    /**
     * Indicates that this input value is required. The DataFlow engine will verify that the input value is present
     * before calling the component.
     */
    boolean required() default true;
}

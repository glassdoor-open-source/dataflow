/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurablePropertyMetadataBuilder.java
 */

package dataflow.annotation.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.annotation.metadata.DataFlowConfigurablePropertyMetadata;

import static dataflow.annotation.processor.AnnotationProcessorUtil.printAndThrowError;

/**
 * Builds the {@link DataFlowConfigurablePropertyMetadata} for a component.
 */
class DataFlowConfigurablePropertyMetadataBuilder {

    /**
     * Simple property types that do not need to be built with a builder.
     */
    static final Set<String> SIMPLE_PROPERTY_TYPES = new HashSet<>();

    static {
        SIMPLE_PROPERTY_TYPES.add(Integer.class.getName());
        SIMPLE_PROPERTY_TYPES.add(int.class.getName());
        SIMPLE_PROPERTY_TYPES.add(int[].class.getName());
        SIMPLE_PROPERTY_TYPES.add(Long.class.getName());
        SIMPLE_PROPERTY_TYPES.add(long.class.getName());
        SIMPLE_PROPERTY_TYPES.add(long[].class.getName());
        SIMPLE_PROPERTY_TYPES.add(Float.class.getName());
        SIMPLE_PROPERTY_TYPES.add(float.class.getName());
        SIMPLE_PROPERTY_TYPES.add(float[].class.getName());
        SIMPLE_PROPERTY_TYPES.add(Double.class.getName());
        SIMPLE_PROPERTY_TYPES.add(double.class.getName());
        SIMPLE_PROPERTY_TYPES.add("double[]");
        SIMPLE_PROPERTY_TYPES.add(boolean.class.getName());
        SIMPLE_PROPERTY_TYPES.add(boolean[].class.getName());
        SIMPLE_PROPERTY_TYPES.add(Boolean.class.getName());
        SIMPLE_PROPERTY_TYPES.add(Enum.class.getName());
        SIMPLE_PROPERTY_TYPES.add(String.class.getName());
        SIMPLE_PROPERTY_TYPES.add(Object.class.getName());
    }

    private ProcessingEnvironment processingEnv;

    DataFlowConfigurablePropertyMetadataBuilder(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    List<DataFlowConfigurablePropertyMetadata> buildPropertyMetadata(final TypeElement classType) {
        String type = classType.asType().toString();
        List<Element> constructors = classType.getEnclosedElements().stream()
                .filter(e -> e.getKind() == ElementKind.CONSTRUCTOR)
                .filter(e -> e.getModifiers().contains(Modifier.PUBLIC))
                .filter(e -> e.getAnnotation(DataFlowConfigurable.class) != null)
                .collect(Collectors.toList());

        if (constructors.size() != 1) {
            printAndThrowError(type + " must have a single public constructor annotated with " +
                    DataFlowConfigurable.class.getSimpleName(), classType, processingEnv);
        }

        List<DataFlowConfigurablePropertyMetadata> propertyMetadata = new ArrayList<>();
        Element constructor = constructors.get(0);

        List<? extends VariableElement> parameters = ((ExecutableElement) constructor).getParameters();
        Set<String> names = new HashSet<>();
        for (VariableElement param : parameters) {
            String name = param.getSimpleName().toString();
            String rawType = param.asType().toString();

            DataFlowConfigProperty annotation = param.getAnnotation(DataFlowConfigProperty.class);
            if (annotation == null) {
                printAndThrowError(type + " constructor arguments must be annotated with " +
                        DataFlowConfigProperty.class.getSimpleName(), classType, processingEnv);
                return propertyMetadata;
            }
            if (annotation.name().length() > 0) {
                // If a name was specified in the annotation then use that instead of the constructor param name.
                name = annotation.name();
            }

            if (names.contains(name)) {
                printAndThrowError(type + " property names must be unique", classType, processingEnv);
            }
            names.add(name);

            TypeElement paramType = processingEnv.getElementUtils().getTypeElement(param.asType().toString());
            boolean isEnum = paramType != null && paramType.getKind() == ElementKind.ENUM;
            propertyMetadata.add(new DataFlowConfigurablePropertyMetadata(
                    name, rawType, isEnum, annotation.supportedTypes(), annotation.required(), annotation.description()));
        }
        return propertyMetadata;
    }
}

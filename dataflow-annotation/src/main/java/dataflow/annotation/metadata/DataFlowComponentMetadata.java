/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadata.java
 */

package dataflow.annotation.metadata;

import java.util.List;
import java.util.Map;

/**
 * Holds information about a DataFlow component.
 */
public class DataFlowComponentMetadata {

    private final String typeName;
    private final String description;
    private final String componentPackageName;
    private final String componentClassName;
    private final Map<String, DataFlowConfigurablePropertyMetadata> propertyMetadataMap;
    private final List<DataFlowComponentInputMetadata> inputMetadata;
    private final DataFlowComponentOutputMetadata outputMetadata;
    /**
     * If not null the component is a {@link dataflow.annotation.ValueConverter} that can be used to
     * convert values from one type to another, null otherwise.
     */
    private final ValueConverterMetadata valueConverterMetadata;

    // The component has async set in the annotation
    private final boolean isAsync;
    // The component has a method that returns a CompletableFuture
    private final boolean hasAsyncGet;
    // The component takes all inputs from the config as a map. This means that new inputs can be added dynamically
    // in configuration without being known in advance.
    private final boolean hasDynamicInput;

    public DataFlowComponentMetadata(final String typeName, final String description, final String componentPackageName,
            final String componentClassName, final Map<String, DataFlowConfigurablePropertyMetadata> propertyMetadataMap,
            final List<DataFlowComponentInputMetadata> inputMetadata, final DataFlowComponentOutputMetadata outputMetadata,
            final ValueConverterMetadata valueConverterMetadata, final boolean isAsync, final boolean hasAsyncGet,
            final boolean hasDynamicInput) {
        this.typeName = typeName;
        this.description = description;
        this.componentPackageName = componentPackageName;
        this.componentClassName = componentClassName;
        this.propertyMetadataMap = propertyMetadataMap;
        this.inputMetadata = inputMetadata;
        this.outputMetadata = outputMetadata;
        this.valueConverterMetadata = valueConverterMetadata;
        this.isAsync = isAsync;
        this.hasAsyncGet = hasAsyncGet;
        this.hasDynamicInput = hasDynamicInput;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDescription() {
        return description;
    }

    public String getComponentPackageName() {
        return componentPackageName;
    }

    public String getComponentClassName() {
        return componentClassName;
    }

    public Map<String, DataFlowConfigurablePropertyMetadata> getPropertyMetadataMap() {
        return propertyMetadataMap;
    }

    public List<DataFlowComponentInputMetadata> getInputMetadata() {
        return inputMetadata;
    }

    public DataFlowComponentOutputMetadata getOutputMetadata() {
        return outputMetadata;
    }

    public boolean isAsync() {
        return isAsync;
    }

    public boolean hasAsyncGet() {
        return hasAsyncGet;
    }

    /**
     * See {@link #hasDynamicInput}
     */
    public boolean hasDynamicInput() {
        return hasDynamicInput;
    }

    public boolean isValueConverter() {
        return valueConverterMetadata != null;
    }

    public ValueConverterMetadata getValueConverterMetadata() {
        return valueConverterMetadata;
    }

    @Override
    public String toString() {
        return "DataFlowComponentMetadata{" +
                "typeName='" + typeName + '\'' +
                ", description='" + description + '\'' +
                ", componentPackageName='" + componentPackageName + '\'' +
                ", componentClassName='" + componentClassName + '\'' +
                ", propertyMetadataMap=" + propertyMetadataMap +
                ", inputMetadata=" + inputMetadata +
                ", outputMetadata=" + outputMetadata +
                ", isAsync=" + isAsync +
                ", hasAsyncGet=" + hasAsyncGet +
                ", hasDynamicInput=" + hasDynamicInput +
                ", isValueConverter=" + (valueConverterMetadata != null) +
                ", valueConverterMetadata=" + valueConverterMetadata +
                '}';
    }
}

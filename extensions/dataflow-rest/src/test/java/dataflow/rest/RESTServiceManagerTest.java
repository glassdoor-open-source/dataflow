/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTServiceManagerTest.java
 */

package dataflow.rest;

import static dataflow.core.datasource.DataSourceUtil.getClassPathDataSourcesMatchingGlobPattern;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import dataflow.rest.RESTServiceManager.RESTServiceRegistration;
import org.junit.Before;
import org.junit.Test;

public class RESTServiceManagerTest {

    private RESTServiceManager manager;

    @Before
    public void setUp() {
        manager = new RESTServiceManager();
    }

    @Test
    public void testRegisterConfigProvider() {
        manager.registerConfigProvider(() ->
            getClassPathDataSourcesMatchingGlobPattern("/test-rest-service-config.yaml"));
        assertEquals(2, manager.getAllServiceRegistrations().size());
        RESTServiceRegistration registration1 = manager.get("testService1");
        assertEquals("http://localhost:8080", registration1.getBasePath());
        assertNotNull(registration1.getRestTemplate());
        RESTServiceRegistration registration2 = manager.get("testService2");
        assertEquals("http://localhost:8081", registration2.getBasePath());
        assertNotNull(registration2.getRestTemplate());
    }
}
/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  KeyBasedResultIterator.java
 */

package dataflow.sql;

import dataflow.core.data.DataRecord;
import dataflow.core.data.DataRecords;
import dataflow.core.data.query.DataFilter;
import dataflow.core.data.query.DataQueryValueProvider;
import dataflow.core.data.query.KeyRangeDataFilter;
import dataflow.core.data.query.MaxRowCountDataFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Used to implement {@link SQLDatabaseIterationMode#KEY_BASED} result iteration.
 */
public class KeyBasedResultIterator implements Iterator<DataRecords> {

    private final Map<String, Object> paramValues;
    private final List<DataFilter> filters;
    private final int batchSize;
    private final DataQueryValueProvider valueProvider;
    private Object fromKey;
    private DataRecords next;

    KeyBasedResultIterator(Map<String, Object> paramValues,
        List<DataFilter> filters, Object fromKey, int batchSize, DataQueryValueProvider valueProvider) {
        this.paramValues = paramValues;
        this.filters = filters;
        this.batchSize = batchSize;
        this.valueProvider = valueProvider;
        this.fromKey = fromKey;
    }

    @Override
    public boolean hasNext() {
        if (next == null) {
            loadNext();
        }
        return next != null && !next.isEmpty();
    }

    @Override
    public DataRecords next() {
        DataRecords retVal = next;
        next = null;
        return retVal;
    }

    private void loadNext() {
        try {
            List<DataFilter> filtersWithKeyFilter = new ArrayList<>(filters);
            if (fromKey != null) {
                filtersWithKeyFilter.add(new KeyRangeDataFilter(fromKey, false, null, false));
                paramValues.put("fromKey", fromKey);
            }
            filtersWithKeyFilter.add(new MaxRowCountDataFilter(batchSize));
            paramValues.put("batchSize", batchSize);
            next = valueProvider.query(paramValues, filtersWithKeyFilter);
            if(next != null && !next.isEmpty()) {
                DataRecord lastRecord = next.get(next.size() - 1);
                fromKey = next.getKey(lastRecord);
            }
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}

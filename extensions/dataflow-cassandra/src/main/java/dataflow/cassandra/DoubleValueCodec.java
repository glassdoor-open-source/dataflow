/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DoubleValueCodec.java
 */

package dataflow.cassandra;

import java.nio.ByteBuffer;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import dataflow.core.value.DoubleValue;

/**
 * A {@link TypeCodec} for DataFlow {@link DoubleValue}.
 */
public class DoubleValueCodec extends TypeCodec<DoubleValue> {

    public static final DoubleValueCodec INSTANCE = new DoubleValueCodec();

    private DoubleValueCodec() {
        super(DataType.cdouble(), DoubleValue.class);
    }

    @Override
    public ByteBuffer serialize(final DoubleValue value,
            final ProtocolVersion protocolVersion) throws InvalidTypeException {
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putDouble(0, value.toDouble());
        return bb;
    }

    @Override
    public DoubleValue deserialize(final ByteBuffer bytes, final ProtocolVersion protocolVersion)
            throws InvalidTypeException {
        if (bytes == null || bytes.remaining() == 0)
            return new DoubleValue();
        if (bytes.remaining() != 8)
            throw new InvalidTypeException(
                    "Invalid 64-bits double value, expecting 8 bytes but got " + bytes.remaining());
        return new DoubleValue(bytes.getDouble(bytes.position()));
    }

    @Override
    public DoubleValue parse(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) {
            return null;
        }
        try {
            DoubleValue retVal = new DoubleValue();
            retVal.fromString(value);
            return retVal;
        } catch (NumberFormatException e) {
            throw new InvalidTypeException(String.format("Cannot parse 64-bits double value from \"%s\"", value));
        }
    }

    @Override
    public String format(DoubleValue value) {
        if (value == null) return "NULL";
        return value.toString();
    }
}

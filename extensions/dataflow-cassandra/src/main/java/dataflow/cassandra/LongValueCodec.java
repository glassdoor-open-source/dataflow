/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LongValueCodec.java
 */

package dataflow.cassandra;

import java.nio.ByteBuffer;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import dataflow.core.value.LongValue;

/**
 * A {@link TypeCodec} for DataFlow {@link LongValue}.
 */
public class LongValueCodec extends TypeCodec<LongValue> {

    public static final LongValueCodec INSTANCE = new LongValueCodec();

    private LongValueCodec() {
        super(DataType.bigint(), LongValue.class);
    }

    @Override
    public ByteBuffer serialize(final LongValue value,
            final ProtocolVersion protocolVersion) throws InvalidTypeException {
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.putLong(0, value.toLong());
        return bb;
    }

    @Override
    public LongValue deserialize(final ByteBuffer bytes, final ProtocolVersion protocolVersion)
            throws InvalidTypeException {
        if (bytes == null || bytes.remaining() == 0)
            return new LongValue();
        if (bytes.remaining() != 8)
            throw new InvalidTypeException(
                    "Invalid 64-bits long value, expecting 8 bytes but got " + bytes.remaining());
        return new LongValue(bytes.getLong(bytes.position()));
    }

    @Override
    public LongValue parse(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) {
            return null;
        }
        try {
            LongValue retVal = new LongValue();
            retVal.fromString(value);
            return retVal;
        } catch (NumberFormatException e) {
            throw new InvalidTypeException(String.format("Cannot parse 64-bits long value from \"%s\"", value));
        }
    }

    @Override
    public String format(LongValue value) {
        if (value == null) return "NULL";
        return value.toString();
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BooleanValueCodecTest.java
 */

package dataflow.cassandra;

import org.junit.Test;
import com.datastax.driver.core.TypeCodec;
import dataflow.core.value.BooleanValue;

import static org.junit.Assert.*;

public class BooleanValueCodecTest extends AbstractTypeCodecTest<BooleanValue> {

    private static final boolean TEST_VALUE = true;

    @Override
    TypeCodec<BooleanValue> getTypeCodec() {
        return BooleanValueCodec.INSTANCE;
    }

    @Override
    BooleanValue getTestValue() {
        return new BooleanValue(TEST_VALUE);
    }

    @Override
    String getTestValueAsString() {
        return String.valueOf(TEST_VALUE);
    }

    @Override
    BooleanValue getEmptyValue() {
        return new BooleanValue();
    }

    @Test
    public void testParse_invalidString() {
        assertFalse(getTypeCodec().parse("test").toBoolean());
    }
}
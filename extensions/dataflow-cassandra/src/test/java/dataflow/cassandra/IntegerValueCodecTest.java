/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerValueCodecTest.java
 */

package dataflow.cassandra;

import com.datastax.driver.core.TypeCodec;
import dataflow.core.value.IntegerValue;

public class IntegerValueCodecTest extends AbstractTypeCodecTest<IntegerValue> {

    private static final int TEST_VALUE = 12345;

    @Override
    TypeCodec<IntegerValue> getTypeCodec() {
        return IntegerValueCodec.INSTANCE;
    }

    @Override
    IntegerValue getTestValue() {
        return new IntegerValue(TEST_VALUE);
    }

    @Override
    String getTestValueAsString() {
        return String.valueOf(TEST_VALUE);
    }

    @Override
    IntegerValue getEmptyValue() {
        return new IntegerValue();
    }
}
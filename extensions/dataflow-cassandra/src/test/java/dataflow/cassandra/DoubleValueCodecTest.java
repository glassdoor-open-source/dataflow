/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DoubleValueCodecTest.java
 */

package dataflow.cassandra;

import com.datastax.driver.core.TypeCodec;
import dataflow.core.value.DoubleValue;

public class DoubleValueCodecTest extends AbstractTypeCodecTest<DoubleValue> {

    private static final double TEST_VALUE = .123456789;


    @Override
    TypeCodec<DoubleValue> getTypeCodec() {
        return DoubleValueCodec.INSTANCE;
    }

    @Override
    DoubleValue getTestValue() {
        return new DoubleValue(TEST_VALUE);
    }

    @Override
    String getTestValueAsString() {
        return String.valueOf(TEST_VALUE);
    }

    @Override
    DoubleValue getEmptyValue() {
        return new DoubleValue();
    }
}
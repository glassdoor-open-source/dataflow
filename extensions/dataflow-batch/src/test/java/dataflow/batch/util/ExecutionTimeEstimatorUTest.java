/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExecutionTimeEstimatorUTest.java
 */

package dataflow.batch.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for {@link ExecutionTimeEstimator}.
 */
public class ExecutionTimeEstimatorUTest {

    @Test
    public void testGetTimeEstimate_singleHistoryRecord() {
        ExecutionTimeEstimator timeEstimator = new ExecutionTimeEstimator();
        timeEstimator.recordExecutionTime(100, 50 * 1000);
        assertEquals(2500, timeEstimator.getRemainingTimeEstimateSeconds(5000));
    }

    @Test
    public void testGetTimeEstimate_multipleHistoryRecords() {
        ExecutionTimeEstimator timeEstimator = new ExecutionTimeEstimator();
        timeEstimator.recordExecutionTime(100, 50 * 1000);
        timeEstimator.recordExecutionTime(100, 10 * 1000);
        assertEquals(300, timeEstimator.getRemainingTimeEstimateSeconds(1000));
    }

    @Test
    public void testGetTimeEstimate_maxHistoryRecords() {
        ExecutionTimeEstimator timeEstimator = new ExecutionTimeEstimator(3);
        timeEstimator.recordExecutionTime(100, 1000 * 1000);
        timeEstimator.recordExecutionTime(100, 50 * 1000);
        timeEstimator.recordExecutionTime(200, 20 * 1000);
        timeEstimator.recordExecutionTime(300, 90 * 1000);
        assertEquals(300, timeEstimator.getRemainingTimeEstimateSeconds(1000));
    }
}
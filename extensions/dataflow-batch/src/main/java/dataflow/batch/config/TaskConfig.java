/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TaskConfig.java
 */

package dataflow.batch.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dataflow.core.parser.DataFlowConfig;
import dataflow.core.retry.Retry;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TaskConfig {

    private Integer batchSize;
    private Integer parallelism;
    private Integer prefetchCount;
    private DataFlowConfig beforeStartFlow;
    private DataFlowConfig afterCompleteFlow;
    private Retry retry;
    private Map<String, Object> properties = new HashMap<>();
    private Map<String, Object> dryRunProperties = new HashMap<>();

    public DataFlowConfig getAfterCompleteFlow() {
        return afterCompleteFlow;
    }

    public void setAfterCompleteFlow(DataFlowConfig onCompleteFlow) {
        this.afterCompleteFlow = onCompleteFlow;
    }

    public Integer getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(Integer batchSize) {
        this.batchSize = batchSize;
    }

    public Integer getParallelism() {
        return parallelism;
    }

    public void setParallelism(Integer parallelism) {
        this.parallelism = parallelism;
    }

    public Integer getPrefetchCount() {
        return prefetchCount;
    }

    public void setPrefetchCount(Integer prefetchCount) {
        this.prefetchCount = prefetchCount;
    }

    public DataFlowConfig getBeforeStartFlow() {
        return beforeStartFlow;
    }

    public void setBeforeStartFlow(DataFlowConfig beforeStartFlow) {
        this.beforeStartFlow = beforeStartFlow;
    }

    public Retry getRetry() {
        return retry;
    }

    public void setRetry(Retry retry) {
        this.retry = retry;
    }

    @JsonIgnore
    public void mergePropertiesIfNotSet(TaskConfig taskConfig) {
        if (taskConfig == null) {
            return;
        }
        batchSize = batchSize != null ? batchSize : taskConfig.batchSize;
        parallelism = parallelism != null ? parallelism : taskConfig.parallelism;
        prefetchCount = prefetchCount != null ? prefetchCount : taskConfig.prefetchCount;
        beforeStartFlow = beforeStartFlow != null ? beforeStartFlow : taskConfig.beforeStartFlow;
        afterCompleteFlow = afterCompleteFlow != null ? afterCompleteFlow : taskConfig.afterCompleteFlow;
        retry = retry != null ? retry : Retry.clone(taskConfig.retry);

        properties = mergeProperties(properties, taskConfig.properties);
        dryRunProperties = mergeProperties(dryRunProperties, taskConfig.dryRunProperties);
    }


    private Map<String, Object> mergeProperties(Map<String, Object> m1, Map<String, Object> m2) {
        Map<String, Object> mergedProperties = m1 != null ? new HashMap<>(m1) : new HashMap<>();
        if(m2 != null) {
            m2.forEach(mergedProperties::putIfAbsent);
        }
        return mergedProperties;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public Map<String, Object> getDryRunProperties() {
        return dryRunProperties;
    }

    public void setDryRunProperties(Map<String, Object> dryRunProperties) {
        this.dryRunProperties = dryRunProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TaskConfig that = (TaskConfig) o;
        return Objects.equals(getBatchSize(), that.getBatchSize()) && Objects
            .equals(getParallelism(), that.getParallelism()) && Objects
            .equals(getPrefetchCount(), that.getPrefetchCount()) && Objects
            .equals(getBeforeStartFlow(), that.getBeforeStartFlow()) && Objects
            .equals(getAfterCompleteFlow(), that.getAfterCompleteFlow()) && Objects
            .equals(getRetry(), that.getRetry()) && Objects
            .equals(getProperties(), that.getProperties()) && Objects
            .equals(getDryRunProperties(), that.getDryRunProperties());
    }

    @Override
    public int hashCode() {
        return Objects
            .hash(getBatchSize(), getParallelism(), getPrefetchCount(), getBeforeStartFlow(),
                getAfterCompleteFlow(), getRetry(), getProperties(), getDryRunProperties());
    }

    @Override
    public String toString() {
        return "TaskConfig{" +
            "batchSize=" + batchSize +
            ", parallelism=" + parallelism +
            ", prefetchCount=" + prefetchCount +
            ", beforeStartFlow=" + beforeStartFlow +
            ", afterCompleteFlow=" + afterCompleteFlow +
            ", retry=" + retry +
            ", properties=" + properties +
            ", dryRunProperties=" + dryRunProperties +
            '}';
    }
}

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExecutionTimeEstimator.java
 */

package dataflow.batch.util;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Used to estimate the remaining time to complete a job. This implementation maintains a constant
 * length history and weights all history equally when calculating the estimate.
 */
public class ExecutionTimeEstimator {

    private static class ExecutionTimeRecord {

        private int itemsProcessed;
        private long executionTimeMillis;

        public ExecutionTimeRecord(final int itemsProcessed, final long executionTimeMillis) {
            this.itemsProcessed = itemsProcessed;
            this.executionTimeMillis = executionTimeMillis;
        }
    }

    private final int maxHistorySize;
    private final Deque<ExecutionTimeRecord> executionTimeHistory;

    public ExecutionTimeEstimator() {
        this(100);
    }

    public ExecutionTimeEstimator(final int maxHistorySize) {
        this.maxHistorySize = maxHistorySize;
        this.executionTimeHistory = new ArrayDeque<>(maxHistorySize);
    }

    public void recordExecutionTime(int itemsProcessed, long executionTimeMillis) {
        if (itemsProcessed > 0 && executionTimeMillis > 0) {
            executionTimeHistory
                .addLast(new ExecutionTimeRecord(itemsProcessed, executionTimeMillis));
            while (executionTimeHistory.size() > maxHistorySize) {
                executionTimeHistory.removeFirst();
            }
        }
    }

    public double getAverageTimePerItemInMillis() {
        double averageTimePerItemMS = 0;
        for (ExecutionTimeRecord record : executionTimeHistory) {
            averageTimePerItemMS += ((double) record.executionTimeMillis / record.itemsProcessed);
        }
        averageTimePerItemMS /= executionTimeHistory.size();
        return averageTimePerItemMS;
    }

    public long getRemainingTimeEstimateSeconds(long itemsRemaining) {
        return (long) (itemsRemaining * getAverageTimePerItemInMillis() / 1000.0d);
    }
}
/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AvroDataRecordWriter.java
 */

package dataflow.avro;

import dataflow.annotation.DataFlowConfigProperty;
import dataflow.annotation.DataFlowConfigurable;
import dataflow.core.data.DataRecord;
import dataflow.core.data.DataRecordWriter;
import dataflow.core.datasource.DataSource;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;

public class AvroDataRecordWriter implements DataRecordWriter {

    private final Schema schema;
    private final DataFileWriter<GenericRecord> writer;

    @DataFlowConfigurable
    public AvroDataRecordWriter(
        @DataFlowConfigProperty String schema,
        @DataFlowConfigProperty DataSource dataSource) {
        this.schema = new Schema.Parser().parse(schema);
        try {
            writer = new DataFileWriter<>(new GenericDatumWriter<>());
            writer.create(this.schema, dataSource.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public AvroDataRecordWriter(Schema schema, OutputStream out) throws IOException {
        this.schema = schema;
        writer = new DataFileWriter<>(new GenericDatumWriter<>());
        writer.create(this.schema, out);
    }

    @Override
    public void write(DataRecord record) throws IOException {
        GenericRecord r = new GenericData.Record(schema);
        r.getSchema().getFields().forEach(f -> r.put(f.name(), normalizeType(record.get(f.name()))));
        writer.append(r);
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    private Object normalizeType(Object obj) {
        if(obj == null) {
            return obj;
        }
        if(obj.getClass().isArray()) {
            obj = Arrays.asList((Object[]) obj);
        }
        return obj;
    }
}

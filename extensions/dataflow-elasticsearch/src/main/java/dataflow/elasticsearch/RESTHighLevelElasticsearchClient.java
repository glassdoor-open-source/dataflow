/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTHighLevelElasticsearchClient.java
 */

package dataflow.elasticsearch;

import java.io.IOException;
import java.util.List;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptRequest;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.PutStoredScriptRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;

public class RESTHighLevelElasticsearchClient implements ElasticsearchClient {

    protected final RestHighLevelClient restHighLevelClient;

    public RESTHighLevelElasticsearchClient(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    public RestHighLevelClient getRestHighLevelClient() {
        return restHighLevelClient;
    }

    @Override
    public String getEndpoint() {
        List<Node> n = restHighLevelClient.getLowLevelClient().getNodes();
        if (n == null || n.size() == 0) {
            return "UNDEFINED";
        }
        return n.get(0).getHost().getHostName();
    }

    @Override
    public void indices_forcemergeAsync(ForceMergeRequest req, ActionListener<ForceMergeResponse> listener) {
        restHighLevelClient.indices().forcemergeAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public ClusterHealthResponse cluster_health() throws IOException {
        return restHighLevelClient.cluster().health(new ClusterHealthRequest(), RequestOptions.DEFAULT);
    }

    public void indices_updateSettings(UpdateSettingsRequest request, RequestOptions requestOptions)
        throws IOException {
        restHighLevelClient.indices().putSettings(request, requestOptions);
    }

    @Override
    public void indexAsync(IndexRequest req, ActionListener listener) {
        restHighLevelClient.indexAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public IndexResponse indexSync(IndexRequest req) throws IOException {
        return restHighLevelClient.index(req, RequestOptions.DEFAULT);
    }


    @Override
    public void bulkAsync(BulkRequest req, ActionListener<BulkResponse> listener) {
        restHighLevelClient.bulkAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public BulkResponse bulk(BulkRequest req, RequestOptions options) throws IOException {
        return restHighLevelClient.bulk(req, options);
    }

    @Override
    public AcknowledgedResponse indices_updateAliases(IndicesAliasesRequest req)
        throws IOException {
        return restHighLevelClient.indices().updateAliases(req, RequestOptions.DEFAULT);
    }

    @Override
    public GetAliasesResponse indices_getAlias(GetAliasesRequest req) throws IOException {
        return restHighLevelClient.indices().getAlias(req, RequestOptions.DEFAULT);
    }

    @Override
    public Response lowLevelClientCall(Request req) throws IOException {
        return restHighLevelClient.getLowLevelClient().performRequest(req);
    }

    @Override
    public SearchResponse search(SearchRequest req) throws IOException {
        return restHighLevelClient.search(req, RequestOptions.DEFAULT);
    }

    @Override
    public SearchResponse search(SearchRequest req, RequestOptions requestOptions) throws IOException {
        return restHighLevelClient.search(req, requestOptions);
    }

    @Override
    public MultiSearchResponse msearch(MultiSearchRequest req, RequestOptions requestOptions) throws IOException{
        return restHighLevelClient.msearch(req, requestOptions);
    }

    @Override
    public GetResponse get(GetRequest req, RequestOptions requestOptions) throws IOException {
        return restHighLevelClient.get(req, requestOptions);
    }

    @Override
    public DeleteResponse delete(DeleteRequest req) throws IOException {
        return restHighLevelClient.delete(req, RequestOptions.DEFAULT);
    }

    @Override
    public AcknowledgedResponse indices_delete(DeleteIndexRequest req) throws IOException {
        return restHighLevelClient.indices().delete(req, RequestOptions.DEFAULT);
    }

    @Override
    public boolean indices_exists(GetIndexRequest req) throws IOException {
        return restHighLevelClient.indices().exists(req, RequestOptions.DEFAULT);
    }

    @Override
    public CreateIndexResponse indices_create(CreateIndexRequest req) throws IOException {
        return restHighLevelClient.indices().create(req, RequestOptions.DEFAULT);
    }

    @Override
    public AcknowledgedResponse putScript(PutStoredScriptRequest req) throws IOException {
        return restHighLevelClient.putScript(req, RequestOptions.DEFAULT);
    }

    @Override
    public GetStoredScriptResponse getScript(GetStoredScriptRequest req) throws IOException {
        return restHighLevelClient.getScript(req, RequestOptions.DEFAULT);
    }
}
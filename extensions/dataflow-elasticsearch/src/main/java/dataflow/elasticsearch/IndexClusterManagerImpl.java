/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndexClusterManagerImpl.java
 */

package dataflow.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceEventListener;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.exception.DataFlowConfigurationException;
import io.micrometer.core.instrument.MeterRegistry;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

public class IndexClusterManagerImpl implements IndexClusterManager, AutoCloseable {

    private class EventListener implements DataSourceEventListener<DataSource> {

        @Override
        public void onAdd(DataSource dataSource) {
            try {
                loadConfig(dataSource);
            } catch (IOException e) {
                onError(e);
            }
        }

        @Override
        public void onChange(DataSource dataSource) {
            try {
                loadConfig(dataSource);
            } catch (IOException e) {
                onError(e);
            }
        }

        @Override
        public void onRemove(DataSource dataSource) {
            clusterIdRegistrationMap.clear();
        }

        @Override
        public void onError(Exception error) {
            // TODO(thorntonv): Implement method.
            error.printStackTrace();
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(IndexClusterManagerImpl.class);

    private Map<String, ElasticsearchClusterRegistration> clusterIdRegistrationMap =
        new ConcurrentHashMap<>();

    private final ObjectMapper objectMapper;

    private final DataSourceManager<DataSource> dataSourceManager;
    private long refreshIntervalSeconds = 300;

    private ScheduledExecutorService metricsExecutor = Executors.newScheduledThreadPool(1);

    public IndexClusterManagerImpl(String metricNamePrefix, MeterRegistry meterRegistry, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.dataSourceManager = new DataSourceManager<>();
        dataSourceManager.addEventListener(new EventListener());

        metricsExecutor.scheduleWithFixedDelay(() -> {
            for(Map.Entry<String, ElasticsearchClusterRegistration> entry : clusterIdRegistrationMap.entrySet()) {
                try {
                    logger.info("Updating Elasticsearch metrics");
                    IndexManager indexManager = entry.getValue().getIndexManager();
                    RestHighLevelClient restHighLevelClient = entry.getValue()
                        .getRestHighLevelClient();
                    ElasticsearchStats.updateIndexMetadataStats(metricNamePrefix, indexManager,
                        restHighLevelClient, meterRegistry);
                    ElasticsearchStats.updateIndexStats(metricNamePrefix, restHighLevelClient, meterRegistry);
                    ElasticsearchStats.updateNodeStats(metricNamePrefix, restHighLevelClient, indexManager, meterRegistry);
                    ElasticsearchStats.updateDiskStats(metricNamePrefix, restHighLevelClient, meterRegistry);
                    logger.info("Finished updating Elasticsearch metrics");
                } catch (IOException | IndexerException e) {
                    logger.warn("Error updating Elasticsearch metrics", e);
                }
            }

        }, 5, 60, TimeUnit.SECONDS);
    }

    public void registerConfigProvider(Callable<List<DataSource>> configProvider) {
        dataSourceManager.register(configProvider, refreshIntervalSeconds);
    }

    public IndexManager getIndexManager(String clusterId) {
        ElasticsearchClusterRegistration registration = clusterIdRegistrationMap.get(clusterId);
        return registration != null ? registration.getIndexManager() : null;
    }

    @Override
    public Map<String, ElasticsearchClusterRegistration> getClusterIdRegistrationMap() {
        return new HashMap<>(clusterIdRegistrationMap);
    }

    @Override
    public void close() throws Exception {
        metricsExecutor.shutdown();
        metricsExecutor.awaitTermination(1, TimeUnit.MINUTES);
    }

    private void loadConfig(DataSource dataSource) throws IOException {
        Yaml yaml = new Yaml();
        Map<String, Object> config = yaml.load(dataSource.getInputStream());
        Map<String, ElasticsearchClusterRegistration> newRegistrationMap = new HashMap<>();
        config.forEach((clusterId, clusterConfig) -> {
            if(clusterConfig instanceof Map) {
                Map<String, Object> clusterConfigMap = (Map<String, Object>) clusterConfig;
                String endpoint = (String) clusterConfigMap.get("endpoint");
                if(endpoint == null) {
                    throw new DataFlowConfigurationException(
                        "No endpoint specified for elasticsearch cluster " + clusterId + ": " + config);
                }
                Map<String, String> typeActiveAliasNameMap =
                    (Map<String, String>) clusterConfigMap.get("indexTypeActiveAliasNames");
                int defaultNumPriorSchemaVersionsToKeep =
                    (int) clusterConfigMap.getOrDefault("defaultNumPriorSchemaVersionsToKeep", 2);
                int defaultNumActiveBackupsForCurrentVersion =
                    (int) clusterConfigMap.getOrDefault("defaultNumActiveBackupsForCurrentVersion", 3);
                int defaultMinIndexAgeForCleanupHours =
                    (int) clusterConfigMap.getOrDefault("defaultMinIndexAgeForCleanupHours", 48);

                RESTHighLevelElasticsearchClient client = buildElasticsearchClient(clusterConfigMap);
                IndexManager indexManager = new IndexManager(clusterId, typeActiveAliasNameMap,
                    objectMapper, client, defaultNumPriorSchemaVersionsToKeep,
                    defaultNumActiveBackupsForCurrentVersion, defaultMinIndexAgeForCleanupHours);
                indexManager.checkClusterHealth();
                newRegistrationMap.put(clusterId, new ElasticsearchClusterRegistration(
                    endpoint, client, client.getRestHighLevelClient(), typeActiveAliasNameMap, indexManager));
            } else {
                throw new DataFlowConfigurationException("Invalid configuration for cluster " +
                    clusterId + ": " + clusterConfig);
            }
        });

        clusterIdRegistrationMap = newRegistrationMap;
    }


    private RESTHighLevelElasticsearchClient buildElasticsearchClient(Map<String, Object> config) {
        String endpoint = (String) config.get("endpoint");
        HttpHost host = HttpHost.create(endpoint);
        Integer connectionTimeoutInMs = (Integer) config.getOrDefault("connectionEstablishTimeoutMillis", 5000);
        Integer readTimeoutInMs = (Integer) config.getOrDefault("eachRequestSocketTimeoutMillis", 5000);
        Integer maxConnectionsPerRoute = (Integer) config.getOrDefault("maxConnectionsPerRoute", 25);
        Integer maxTotalConnections = (Integer) config.getOrDefault("maxConnections", 50);

        return new RESTHighLevelElasticsearchClient(new RestHighLevelClient(RestClient.builder(host)
            .setRequestConfigCallback(requestConfigBuilder -> requestConfigBuilder
                .setConnectTimeout(connectionTimeoutInMs)
                .setConnectionRequestTimeout(connectionTimeoutInMs)
                .setSocketTimeout(readTimeoutInMs))
            .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder
                .setMaxConnPerRoute(maxConnectionsPerRoute)
                .setMaxConnTotal(maxTotalConnections))));
    }
}

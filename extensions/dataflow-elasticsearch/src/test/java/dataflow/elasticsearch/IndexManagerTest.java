/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndexManagerTest.java
 */

package dataflow.elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.SocketTimeoutException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkItemResponse.Failure;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse.Builder;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchResponseSections;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.index.shard.ShardId;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class IndexManagerTest {

    private static final String TEST_INDEX_TYPE_1 = "testType1";
    private static final String TEST_INDEX_NAME_1 = "testName1";
    private static final String TEST_VERSION_1 = "1";
    private static final int TEST_SCHEMA_VERSION_1 = 2;
    private static final Date TEST_CREATE_DATE_1 =
        Date.from(LocalDateTime.now().minusDays(30).atZone(ZoneId.systemDefault()).toInstant());
    private static final String TEST_SCHEMA_1 = "{\"mappings\":{\"dynamic_templates\":[{\"strings\":{\"match_mapping_type\":\"string\",\"mapping\":{\"type\":\"keyword\"}}}]}}";
    private static final IndexMetadata TEST_METADATA_1 = buildIndexMetadata(TEST_INDEX_TYPE_1,
        TEST_INDEX_NAME_1, TEST_VERSION_1, TEST_SCHEMA_VERSION_1, IndexingStatusEnum.COMPLETE,
        TEST_CREATE_DATE_1);

    private static final String TEST_INDEX_TYPE_2 = "testType2";
    private static final String TEST_INDEX_NAME_2 = "testName2";
    private static final String TEST_VERSION_2 = "2";
    private static final Date TEST_CREATE_DATE_2 =
        Date.from(LocalDateTime.now().minusDays(15).atZone(ZoneId.systemDefault()).toInstant());
    private static final IndexMetadata TEST_METADATA_2 = buildIndexMetadata(TEST_INDEX_TYPE_2,
        TEST_INDEX_NAME_2, TEST_VERSION_2, TEST_SCHEMA_VERSION_1, IndexingStatusEnum.COMPLETE,
        TEST_CREATE_DATE_2);

    private static final String TEST_INDEX_TYPE_3 = "testType1";
    private static final String TEST_INDEX_NAME_3 = "testName3";
    private static final String TEST_VERSION_3 = "3";
    private static final int TEST_SCHEMA_VERSION_3 = 6;
    private static final Date TEST_CREATE_DATE_3 =
        Date.from(LocalDateTime.now().minusDays(5).atZone(ZoneId.systemDefault()).toInstant());
    private static final IndexMetadata TEST_METADATA_3 = buildIndexMetadata(TEST_INDEX_TYPE_3,
        TEST_INDEX_NAME_3, TEST_VERSION_3, TEST_SCHEMA_VERSION_3, IndexingStatusEnum.COMPLETE,
        TEST_CREATE_DATE_3);

    private static final String TEST_INDEX_TYPE_4 = "testType1";
    private static final String TEST_INDEX_NAME_4 = "testName4";
    private static final String TEST_VERSION_4 = "4";
    private static final Date TEST_CREATE_DATE_4 =
        Date.from(LocalDateTime.now().minusDays(3).atZone(ZoneId.systemDefault()).toInstant());
    private static final IndexMetadata TEST_METADATA_4 = buildIndexMetadata(TEST_INDEX_TYPE_4,
        TEST_INDEX_NAME_4, TEST_VERSION_4, TEST_SCHEMA_VERSION_3, IndexingStatusEnum.COMPLETE,
        TEST_CREATE_DATE_4);

    private static final String TEST_INDEX_TYPE_5 = "testType1";
    private static final String TEST_INDEX_NAME_5 = "testName5";
    private static final String TEST_VERSION_5 = "5";
    private static final Date TEST_CREATE_DATE_5 =
        Date.from(LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant());
    private static final IndexMetadata TEST_METADATA_5 = buildIndexMetadata(TEST_INDEX_TYPE_5,
        TEST_INDEX_NAME_5, TEST_VERSION_5, TEST_SCHEMA_VERSION_3, IndexingStatusEnum.COMPLETE,
        TEST_CREATE_DATE_5);

    private final int defaultNumPriorSchemaVersionsToKeep = 1;
    private final int defaultNumActiveBackupsForCurrentVersion = 1;
    private final int defaultMinIndexAgeForCleanupHours = 4;

    @Mock
    private ElasticsearchClient mockClient;
    @Captor
    private ArgumentCaptor<ActionListener<ForceMergeResponse>> forceMergeListenerCaptor;

    private ObjectMapper objectMapper;
    private IndexManager indexManager;

    @Before
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        this.indexManager = new IndexManager("test", null,
            objectMapper, mockClient, defaultNumPriorSchemaVersionsToKeep,
            defaultNumActiveBackupsForCurrentVersion, defaultMinIndexAgeForCleanupHours);
    }

    @Test
    public void testCreateIndex() throws IOException, IndexerException {
        ArgumentCaptor<CreateIndexRequest> createRequestCaptor = ArgumentCaptor.forClass(CreateIndexRequest.class);
        when(mockClient.indices_create(createRequestCaptor.capture())).thenReturn(
            new CreateIndexResponse(true, true, TEST_INDEX_NAME_1));
        ArgumentCaptor<IndexRequest> indexRequestCaptor = ArgumentCaptor.forClass(IndexRequest.class);
        IndexMetadata metadata = indexManager.createIndex(TEST_INDEX_TYPE_1, TEST_INDEX_NAME_1, TEST_VERSION_1, TEST_SCHEMA_1, TEST_SCHEMA_VERSION_1);

        verify(mockClient).indexAsync(indexRequestCaptor.capture(), any());

        assertEquals(TEST_INDEX_NAME_1.toLowerCase(), createRequestCaptor.getValue().index());
        assertEquals("{\"dynamic_templates\":[{\"strings\":{\"mapping\":{\"type\":\"keyword\"},\"match_mapping_type\":\"string\"}}]}",
            createRequestCaptor.getValue().mappings().utf8ToString());

        assertEquals(TEST_INDEX_TYPE_1, indexRequestCaptor.getValue().sourceAsMap().get("type"));
        assertEquals(TEST_INDEX_NAME_1.toLowerCase(), indexRequestCaptor.getValue().sourceAsMap().get("name"));
        assertEquals(TEST_VERSION_1, indexRequestCaptor.getValue().sourceAsMap().get("version"));
        assertEquals(TEST_SCHEMA_VERSION_1, indexRequestCaptor.getValue().sourceAsMap().get("schemaVersion"));
        assertNotNull(indexRequestCaptor.getValue().sourceAsMap().get("indexCreateDateTime"));

        assertEquals(TEST_INDEX_TYPE_1, metadata.getType());
        assertEquals(TEST_INDEX_NAME_1.toLowerCase(), metadata.getName());
        assertEquals(TEST_VERSION_1, metadata.getVersion());
        assertNotNull(metadata.getIndexCreateDateTime());
    }

    @Test
    public void testGetIndexMetadata() throws IOException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1, TEST_METADATA_2, TEST_METADATA_3));
        List<IndexMetadata> metadataList = indexManager.getIndexMetadata();
        assertEquals(3, metadataList.size());
        assertEquals(TEST_METADATA_3, metadataList.get(0));
        assertEquals(TEST_METADATA_2, metadataList.get(1));
        assertEquals(TEST_METADATA_1, metadataList.get(2));
    }

    @Test
    public void testGetIndexMetadataForAlias() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("test_v2", TEST_INDEX_NAME_1)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));

        assertEquals(TEST_METADATA_1, indexManager.getIndexMetadataForAlias("test_v2").get());
    }

    @Test
    public void testGetActiveIndexMetadata() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_3)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1, TEST_METADATA_3));

        assertEquals(TEST_METADATA_3, indexManager.getActiveIndexMetadata(TEST_INDEX_TYPE_1).get());
    }

    @Test
    public void testGetActiveIndexMetadata_noActiveIndex() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder().build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1, TEST_METADATA_3));
        assertFalse(indexManager.getActiveIndexMetadata(TEST_INDEX_TYPE_1).isPresent());
    }

    @Test
    public void testCleanupOldIndexes_noActiveIndex() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder().build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));
        assertEquals(ImmutableList.of(), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 4));
    }

    @Test
    public void testPhysicalIndexExists() throws IOException {
        mockIndexesExists(ImmutableSet.of(TEST_INDEX_NAME_1));
        assertTrue(indexManager.physicalIndexExists(TEST_METADATA_1));
    }

    @Test
    public void testPhysicalIndexExists_nonExistent() throws IOException {
        assertFalse(indexManager.physicalIndexExists(TEST_METADATA_1));
    }

    @Test
    public void testWriteDocs()
        throws IOException, InterruptedException, ExecutionException, TimeoutException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));
        ArgumentCaptor<BulkRequest> bulkRequestCaptor = ArgumentCaptor.forClass(BulkRequest.class);
        ArgumentCaptor<ActionListener> listenerCaptor = ArgumentCaptor.forClass(ActionListener.class);
        doAnswer((Answer<Object>) invocationOnMock -> {
            List<DocWriteRequest<?>> requests = bulkRequestCaptor.getValue().requests();
            assertEquals(2, requests.size());
            assertEquals("\"A\"", ((IndexRequest)requests.get(0)).source().utf8ToString());
            assertEquals("\"B\"", ((IndexRequest)requests.get(1)).source().utf8ToString());
            BulkResponse response = new BulkResponse(new BulkItemResponse[0], 100);
            listenerCaptor.getValue().onResponse(response);
            return null;
        }).when(mockClient).bulkAsync(bulkRequestCaptor.capture(), listenerCaptor.capture());

        CompletableFuture<Map<String, Object>> futureResult = indexManager.writeDocuments(ImmutableMap.<String, Object>builder()
                .put("1", "A")
                .put("2", "B")
                .build(), TEST_METADATA_1, false, 10000, 1, 1000,
            retryCount -> {});

        futureResult.get(10, TimeUnit.SECONDS);
    }

    @Test
    public void testDeleteDocs()
        throws IOException, InterruptedException, ExecutionException, TimeoutException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));
        ArgumentCaptor<BulkRequest> bulkRequestCaptor = ArgumentCaptor.forClass(BulkRequest.class);
        ArgumentCaptor<ActionListener> listenerCaptor = ArgumentCaptor.forClass(ActionListener.class);
        doAnswer((Answer<Object>) invocationOnMock -> {
            List<DocWriteRequest<?>> requests = bulkRequestCaptor.getValue().requests();
            assertEquals(2, requests.size());
            assertEquals("1", requests.get(0).id());
            assertEquals(TEST_INDEX_NAME_1.toLowerCase(), ((DeleteRequest)requests.get(0)).index());
            assertEquals("2", requests.get(1).id());
            assertEquals(TEST_INDEX_NAME_1.toLowerCase(), ((DeleteRequest)requests.get(1)).index());
            BulkResponse response = new BulkResponse(new BulkItemResponse[0], 100);
            listenerCaptor.getValue().onResponse(response);
            return null;
        }).when(mockClient).bulkAsync(bulkRequestCaptor.capture(), listenerCaptor.capture());

        CompletableFuture<Set<String>> futureResult = indexManager.deleteDocuments(
            ImmutableSet.of("1", "2"), TEST_METADATA_1, false, 10000, 1, 1000,
            retryCount -> {});

        futureResult.get(10, TimeUnit.SECONDS);
    }

    @Test
    public void testWriteDocs_serverErrorResponse() throws IOException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));
        ArgumentCaptor<BulkRequest> bulkRequestCaptor = ArgumentCaptor.forClass(BulkRequest.class);
        ArgumentCaptor<ActionListener> listenerCaptor = ArgumentCaptor.forClass(ActionListener.class);
        doAnswer((Answer<Object>) invocationOnMock -> {
            Failure failure = new Failure(TEST_INDEX_NAME_1, "", "", new Exception("mock exception"));
            BulkItemResponse errorResponse = new BulkItemResponse(1, DocWriteRequest.OpType.INDEX, failure);
            BulkResponse response = new BulkResponse(new BulkItemResponse[]{errorResponse}, 100);
            listenerCaptor.getValue().onResponse(response);
            return null;
        }).when(mockClient).bulkAsync(bulkRequestCaptor.capture(), listenerCaptor.capture());

        CompletableFuture<Map<String, Object>> futureResult = indexManager.writeDocuments(ImmutableMap.<String, Object>builder()
                .put("1", "A")
                .put("2", "B")
                .build(), TEST_METADATA_1, false, 10000, 1, 1000,
            retryCount -> {});

        try {
            futureResult.get(10, TimeUnit.SECONDS);
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("dataflow.elasticsearch.IndexerException: Error performing write: Document failure: java.lang.Exception: mock exception\n", ex.getMessage());
        }
    }

    @Test
    public void testWriteDocs_IOException() throws IOException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));
        ArgumentCaptor<BulkRequest> bulkRequestCaptor = ArgumentCaptor.forClass(BulkRequest.class);
        ArgumentCaptor<ActionListener> listenerCaptor = ArgumentCaptor.forClass(ActionListener.class);
        doAnswer((Answer<Object>) invocationOnMock -> {
            listenerCaptor.getValue().onFailure(new IOException("mock exception"));
            return null;
        }).when(mockClient).bulkAsync(bulkRequestCaptor.capture(), listenerCaptor.capture());

        CompletableFuture<Map<String, Object>> futureResult = indexManager.writeDocuments(ImmutableMap.<String, Object>builder()
                .put("1", "A")
                .put("2", "B")
                .build(), TEST_METADATA_1, false, 10000, 1, 1000,
            retryCount -> {});

        try {
            futureResult.get(10, TimeUnit.SECONDS);
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.io.IOException: mock exception", ex.getMessage());
        }
    }

    @Test
    public void testWriteDocs_socketTimeout() throws IOException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));
        ArgumentCaptor<BulkRequest> bulkRequestCaptor = ArgumentCaptor.forClass(BulkRequest.class);
        ArgumentCaptor<ActionListener> listenerCaptor = ArgumentCaptor.forClass(ActionListener.class);
        AtomicInteger retryCounter = new AtomicInteger(0);
        doAnswer((Answer<Object>) invocationOnMock -> {
            listenerCaptor.getValue().onFailure(new SocketTimeoutException("mock exception"));
            return null;
        }).when(mockClient).bulkAsync(bulkRequestCaptor.capture(), listenerCaptor.capture());

        CompletableFuture<Map<String, Object>> futureResult = indexManager.writeDocuments(ImmutableMap.<String, Object>builder()
                .put("1", "A")
                .put("2", "B")
                .build(), TEST_METADATA_1, false, 10000, 1, 1000,
            retryCount -> retryCounter.incrementAndGet());

        try {
            futureResult.get(10, TimeUnit.SECONDS);
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.net.SocketTimeoutException: mock exception", ex.getMessage());
        }
        assertEquals(1, retryCounter.get());
    }

    @Test
    public void testSwitchIndex() throws IOException, IndexerException {
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_1));

        ArgumentCaptor<IndexRequest> metadataUpdateReqCaptor =
            ArgumentCaptor.forClass(IndexRequest.class);
        IndexResponse response = new IndexResponse(ShardId.fromString("[metadata][1]"), "", "", 0,
            0, 0, false);
        when(mockClient.indexSync(metadataUpdateReqCaptor.capture())).thenReturn(response);

        indexManager.switchIndex(TEST_METADATA_1, 5, TimeUnit.SECONDS, () -> false);

        IndexMetadata updatedMetadata = objectMapper.readValue(
            metadataUpdateReqCaptor.getValue().source().utf8ToString(), IndexMetadata.class);
        assertEquals(100, updatedMetadata.getSwitchPercent());
    }

    @Test
    public void testForceMerge() {
        ArgumentCaptor<ForceMergeRequest> requestCaptor = ArgumentCaptor.forClass(ForceMergeRequest.class);

        doAnswer(invocationOnMock -> {
            new Thread(() -> forceMergeListenerCaptor.getValue().onResponse(
                buildForceMergeResponse())).start();
            return null;
        }).when(mockClient).indices_forcemergeAsync(requestCaptor.capture(), forceMergeListenerCaptor.capture());

        indexManager.forceMergeSegments(TEST_METADATA_3, 5);

        assertEquals(TEST_INDEX_NAME_3.toLowerCase(), requestCaptor.getValue().indices()[0]);
        assertEquals(5, requestCaptor.getValue().maxNumSegments());
    }

    @Test
    public void testForceMerge_error() {
        ArgumentCaptor<ForceMergeRequest> requestCaptor = ArgumentCaptor.forClass(ForceMergeRequest.class);

        doAnswer(invocationOnMock -> {
            new Thread(() -> forceMergeListenerCaptor.getValue().onFailure(
                new Exception("mock exception"))).start();
            return null;
        }).when(mockClient).indices_forcemergeAsync(requestCaptor.capture(), forceMergeListenerCaptor.capture());

        indexManager.forceMergeSegments(TEST_METADATA_3, 5);
    }

    @Test
    public void testSetActiveIndex() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_3)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));

        ArgumentCaptor<IndicesAliasesRequest> requestCaptor =
            ArgumentCaptor.forClass(IndicesAliasesRequest.class);

        when(mockClient.indices_updateAliases(requestCaptor.capture()))
            .thenReturn(new AcknowledgedResponse(true));

        indexManager.makeActiveIndex(TEST_METADATA_4);
        assertEquals(2, requestCaptor.getValue().getAliasActions().size());
        assertEquals("testType1_v6", requestCaptor.getValue().getAliasActions().get(0).aliases()[0]);
        assertEquals(TEST_INDEX_NAME_3.toLowerCase(), requestCaptor.getValue().getAliasActions().get(0).indices()[0]);
        assertEquals("testType1_v6", requestCaptor.getValue().getAliasActions().get(1).aliases()[0]);
        assertEquals(TEST_INDEX_NAME_4.toLowerCase(), requestCaptor.getValue().getAliasActions().get(1).indices()[0]);
    }

    @Test
    public void testRemoveAlias() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_3)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));

        ArgumentCaptor<IndicesAliasesRequest> requestCaptor =
            ArgumentCaptor.forClass(IndicesAliasesRequest.class);

        when(mockClient.indices_updateAliases(requestCaptor.capture()))
            .thenReturn(new AcknowledgedResponse(true));

        indexManager.removeAlias("testType1_v6");
        assertEquals(1, requestCaptor.getValue().getAliasActions().size());
        assertEquals("testType1_v6", requestCaptor.getValue().getAliasActions().get(0).aliases()[0]);
        assertEquals(TEST_INDEX_NAME_3.toLowerCase(), requestCaptor.getValue().getAliasActions().get(0).indices()[0]);
    }

    @Test
    public void testRemoveIndex_activeIndex() throws IOException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_3)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));
        try {
            indexManager.removeIndex(TEST_METADATA_3);
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Attempted to remove the active testType1 index", ex.getMessage());
        }
    }

    @Test
    public void testRemoveIndex() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_4)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));
        mockDropIndexSuccess(ImmutableSet.of(TEST_INDEX_NAME_3));
        mockDropMetadataSuccess(ImmutableSet.of(TEST_INDEX_NAME_3));
        indexManager.removeIndex(TEST_METADATA_3);
    }

    @Test
    public void testCleanupOldIndexes_activeIndexOnly() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_3)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3));
        assertEquals(ImmutableList.of(), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 4));
    }

    @Test
    public void testCleanupOldIndexes_activeIndexAndBackupsOnly() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_4)
            .build());
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3, TEST_METADATA_4));
        assertEquals(ImmutableList.of(), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 4));
    }

    @Test
    public void testCleanupOldIndexes_cleanupOldBackup() throws IOException, IndexerException {
        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_5)
            .build());
        Set<String> indexNames = ImmutableSet.of(TEST_INDEX_NAME_3.toLowerCase(),
            TEST_INDEX_NAME_4.toLowerCase(), TEST_INDEX_NAME_5.toLowerCase());
        mockIndexesExists(indexNames);
        mockDropIndexSuccess(indexNames);
        mockDropMetadataSuccess(indexNames);
        mockGetIndexMetadata(ImmutableList.of(TEST_METADATA_3, TEST_METADATA_4, TEST_METADATA_5));
        assertEquals(ImmutableList.of(TEST_METADATA_3), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 4));
    }

    @Test
    public void testCleanupOldIndexes_minIndexAge() throws IOException, IndexerException {
        IndexMetadata index3 = buildIndexMetadata(TEST_INDEX_TYPE_3,
            TEST_INDEX_NAME_3, TEST_VERSION_3, TEST_SCHEMA_VERSION_3, IndexingStatusEnum.COMPLETE,
            Date.from(LocalDateTime.now().minusDays(4).atZone(ZoneId.systemDefault()).toInstant()));

        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_5)
            .build());
        Set<String> indexNames = ImmutableSet.of(TEST_INDEX_NAME_3, TEST_INDEX_NAME_4, TEST_INDEX_NAME_5);
        mockIndexesExists(indexNames);
        mockDropIndexSuccess(indexNames);
        mockDropMetadataSuccess(indexNames);
        mockGetIndexMetadata(ImmutableList.of(index3, TEST_METADATA_4, TEST_METADATA_5));
        assertEquals(ImmutableList.of(), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 5* 24));
    }

    @Test
    public void testCleanupOldIndexes_incompleteIndex() throws IOException, IndexerException {
        IndexMetadata index3 = buildIndexMetadata(TEST_INDEX_TYPE_3,
            TEST_INDEX_NAME_3, TEST_VERSION_3, TEST_SCHEMA_VERSION_3, IndexingStatusEnum.IN_PROGRESS,
            TEST_METADATA_3.getIndexCreateDateTime());

        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType1_v6", TEST_INDEX_NAME_5)
            .build());
        Set<String> indexNames = ImmutableSet.of(TEST_INDEX_NAME_3, TEST_INDEX_NAME_4, TEST_INDEX_NAME_5);
        mockIndexesExists(indexNames);
        mockDropIndexSuccess(indexNames);
        mockDropMetadataSuccess(indexNames);
        mockGetIndexMetadata(ImmutableList.of(index3, TEST_METADATA_4, TEST_METADATA_5));
        assertEquals(ImmutableList.of(), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_1, 1, 1, 4));
    }

    @Test
    public void testCleanupOldIndexes_cleanupOldSchemaVersion() throws IOException, IndexerException {
        IndexMetadata metadata1 = buildIndexMetadata(TEST_INDEX_TYPE_2,
            TEST_INDEX_NAME_1, TEST_VERSION_2, TEST_SCHEMA_VERSION_1, IndexingStatusEnum.COMPLETE,
            new Date(2021-1900, Calendar.JANUARY, 11));

        IndexMetadata metadata2 = buildIndexMetadata(TEST_INDEX_TYPE_2,
            TEST_INDEX_NAME_2, TEST_VERSION_2, TEST_SCHEMA_VERSION_1+1, IndexingStatusEnum.COMPLETE,
            new Date(2021-1900, Calendar.JANUARY, 11));

        IndexMetadata metadata3 = buildIndexMetadata(TEST_INDEX_TYPE_2,
            TEST_INDEX_NAME_3, TEST_VERSION_2, TEST_SCHEMA_VERSION_1+2, IndexingStatusEnum.COMPLETE,
            new Date(2021-1900, Calendar.JANUARY, 11));

        mockAliases(ImmutableMap.<String, String>builder()
            .put("testType2_v4", metadata3.getName())
            .build());
        Set<String> indexNames = ImmutableSet.of(metadata1.getName(), metadata2.getName(), metadata3.getName());
        mockIndexesExists(indexNames);
        mockDropIndexSuccess(indexNames);
        mockDropMetadataSuccess(indexNames);
        mockGetIndexMetadata(ImmutableList.of(metadata1, metadata2, metadata3));
        assertEquals(ImmutableList.of(metadata1), indexManager.cleanupOldIndexes(
            TEST_INDEX_TYPE_2, 1, 1, 4));
    }

    private void mockDropIndexSuccess(Set<String> indexNames) throws IOException {
        when(mockClient.indices_delete(any())).thenAnswer(invocationOnMock ->
            indexNames.contains(((DeleteIndexRequest) invocationOnMock.getArguments()[0]).indices()[0]) ?
                new AcknowledgedResponse(true) : null);
    }

    private void mockDropMetadataSuccess(Set<String> indexNames) throws IOException {
        when(mockClient.delete(any())).thenAnswer(invocationOnMock -> {
            boolean success = indexNames.contains(((DeleteRequest) invocationOnMock.getArguments()[0]).id());
            Builder responseBuilder = new Builder();
            responseBuilder.setShardId(ShardId.fromString("[metadata][1]"));
            responseBuilder.setType("");
            responseBuilder.setId("");
            responseBuilder.setVersion(1L);
            responseBuilder.setResult(Result.DELETED);
            return success ? responseBuilder.build() : null;
        });
    }

    private static IndexMetadata buildIndexMetadata(String type, String name, String version,
        int schemaVersion, IndexingStatusEnum status, Date createdTime) {
        IndexMetadata indexMetadata = new IndexMetadata();
        indexMetadata.setType(type);
        indexMetadata.setName(name);
        indexMetadata.setVersion(version);
        indexMetadata.setSchemaVersion(schemaVersion);
        indexMetadata.setIndexingStatus(status);
        indexMetadata.setIndexCreateDateTime(createdTime);
        return indexMetadata;
    }

    private void mockGetIndexMetadata(List<IndexMetadata> metadataList) throws IOException {
        ArgumentCaptor<SearchRequest> requestCaptor = ArgumentCaptor.forClass(SearchRequest.class);
        when(mockClient.search(requestCaptor.capture())).thenAnswer(invocationOnMock -> {
            if(Arrays.equals(requestCaptor.getValue().indices(), new String[]{"indexmetadata"})) {
                return buildIndexMetadataSearchResponse(metadataList);
            }
            return null;
        });
    }

    private SearchResponse buildIndexMetadataSearchResponse(List<IndexMetadata> metadataList) {
        List<SearchHit> hits = new ArrayList<>();
        metadataList.forEach(metadata -> {
            try {
                hits.add(new SearchHit(0, metadata.getName(), null, null, null)
                    .sourceRef(new BytesArray(objectMapper.writeValueAsString(metadata))));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
        SearchHits searchHits = new SearchHits(hits.toArray(new SearchHit[0]), null, 1.0f);
        SearchResponseSections sections = new SearchResponseSections(searchHits, null, null, false, null, null, 0);
        return new SearchResponse(sections, null, 1, 1, 0, 100, null, null);
    }

    private void mockAliases(Map<String, String> indexNameAliasMap) throws IOException {
        indexNameAliasMap = indexNameAliasMap.entrySet().stream().collect(Collectors.toMap(k -> k.getKey(), v -> v.getValue().toLowerCase()));
        when(mockClient.indices_getAlias(any())).thenReturn(buildGetAliasesResponse(indexNameAliasMap));
    }

    private GetAliasesResponse buildGetAliasesResponse(Map<String, String> indexNameAliasMap) {
        Map<String, Set<AliasMetadata>> aliases = new HashMap<>();
        indexNameAliasMap.forEach((indexName, alias) -> aliases.put(alias,
            ImmutableSet.of(AliasMetadata.builder(indexName).build())));

        try {
            Constructor constructor = Class.forName(GetAliasesResponse.class.getName())
                .getDeclaredConstructor(RestStatus.class, String.class, Map.class);
            constructor.setAccessible(true);
            return (GetAliasesResponse) constructor.newInstance(RestStatus.ACCEPTED, "", aliases);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private ForceMergeResponse buildForceMergeResponse() {
        try {
            Constructor constructor = Class.forName(ForceMergeResponse.class.getName())
                .getDeclaredConstructor(int.class, int.class, int.class, List.class);
            constructor.setAccessible(true);
            return (ForceMergeResponse) constructor.newInstance(0, 0, 0, Collections.emptyList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void mockIndexesExists(Set<String> indexNames) throws IOException {
        Set<String> indexNamesLowercase = indexNames.stream().map(String::toLowerCase)
            .collect(Collectors.toSet());
        ArgumentCaptor<GetIndexRequest> getRequestCaptor =
            ArgumentCaptor.forClass(GetIndexRequest.class);
        when(mockClient.indices_exists(getRequestCaptor.capture())).thenAnswer(
            invocationOnMock -> indexNamesLowercase.contains(getRequestCaptor.getValue().indices()[0]));
    }
}